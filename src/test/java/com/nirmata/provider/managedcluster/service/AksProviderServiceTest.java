package com.nirmata.provider.managedcluster.service;

import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.PagedList;
import com.microsoft.azure.credentials.ApplicationTokenCredentials;
import com.microsoft.azure.management.compute.VirtualMachine;
import com.microsoft.azure.management.compute.implementation.ComputeManagementClientImpl;
import com.microsoft.azure.management.compute.implementation.UsageInner;
import com.microsoft.azure.management.compute.implementation.VirtualMachineInner;
import com.microsoft.azure.management.compute.implementation.VirtualMachineSizeInner;
import com.nirmata.provider.managedcluster.util.*;
import javafx.util.Pair;
import com.nirmata.provider.managedcluster.BaseClient;
import jdk.nashorn.internal.ir.debug.ObjectSizeCalculator;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import static junit.framework.TestCase.assertTrue;

@FixMethodOrder(MethodSorters.DEFAULT)

public final class AksProviderServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(AksProviderServiceTest.class);
    private static final String clientID = "f1f7b68a-e0c7-4b65-b37a-20a4db1969a9";
    private static final String tenantId = "ad7662fc-adc3-4a94-b4c2-17bfd5a469a0";
    private static final String clientSecret = "dfd10a9c-1067-49d3-ad39-b5cfcb900ec1";
    private static final String subscriptionId = "d8c495da-03dc-4ffc-8d3b-e2348843b1b7";
    private static final AksConfig.Credential credential = new AksConfig.Credential(clientID, clientSecret, tenantId, subscriptionId);
    private static final String clusterName = "cluster-1";
    private static final String resourceGroup = "myResourceGroup"; //Alter it to "anzen-resource" while running all the test.
    private static final String region = "westus";
    private static final String clusterVersion = "1.11.2";
    private static final  BaseProviderService service = AksProviderService.getInstance();
    private static final List<String> regions = Arrays.asList("eastus","westeurope","centralus","canadacentral"
            ,"canadaeast","uksouth","westus","westus2","australiaeast","northeurope","japaneast","eastus2","southeastasia");

    private static final int veryShortSleepDuration = 500;
    private static final int shortSleepDuration = 20000;
    private static final int goodSleepDuration  = 120000;
    private static final int longSleepDuration = 150000;
    private static final int veryLongSleepDuration = 240000;
    private static final int maxAttempts = 10;
    private static final int minSuffixLength = 3;

    private String getRandomCluster(){
        int length = new Random().nextInt(5) +minSuffixLength;
        return "cluster-"+RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }
    private String getRandomResourceGroup(){
        int length = new Random().nextInt(5) +minSuffixLength;
        return "resource-"+RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }
    private String getRandomRegion(List<String> regions){
        return regions.get(new Random().nextInt(regions.size()));
    }
    private int wrapCheckClusterStatusForDelete(AksConfig aksConfig) throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        int noAttempts = maxAttempts;
        while(true){
            if(checkClusterStatus(aksConfig) == BaseProviderService.Status.STOPPING) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }
    private BaseProviderService.Status  checkClusterStatus(AksConfig aksConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        try {
            //The method doesn't throw  exception until there are any networking problems.
            BaseClient client = service.getManagedK8Client(aksConfig);
            return service.getClusterStatus(client);
        }
        catch (IOException  e){
            logger.error(e.getMessage());
        }
        return BaseProviderService.Status.NO_STATUS;
    }
    private boolean  runClusterWithConfig(AksConfig aksConfig) throws IOException, CloneNotSupportedException, GeneralSecurityException {
        BaseClient client = service.getManagedK8Client(aksConfig);

        try {
            return service.createCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    private List<String>  listClustersWithConfig(AksConfig aksConfig) throws IOException, CloneNotSupportedException, GeneralSecurityException {
        BaseClient client = service.getManagedK8Client(aksConfig);

        try {
            return service.listClusters(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    private boolean  scaleClusterWithConfig(AksConfig aksConfig) throws IOException, CloneNotSupportedException, GeneralSecurityException {
        BaseClient client = service.getManagedK8Client(aksConfig);

        try {
            return service.scaleCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }
    private boolean  deleteClusterWithConfig(AksConfig aksConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(aksConfig);

        try {
            return service.deleteCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        }
        return false;
    }

    private boolean deleteMultiThreadedClusters(List<Future<Boolean>> futures, List<AksConfig> aksConfigs) throws InterruptedException, IOException, GeneralSecurityException, TimeoutException, ExecutionException, CloneNotSupportedException {
        for(int index = 0; index <futures.size(); index++) {
            if (futures.get(index).isDone()) {
                assertTrue(futures.get(index).get(veryShortSleepDuration, TimeUnit.MILLISECONDS));
                assertTrue(deleteClusterWithConfig(aksConfigs.get(index)));
            }
            else {
                Thread.sleep(veryShortSleepDuration);
            }
        }
        return true;

    }
    private boolean compareRegionVMList( List<VirtualMachineSize> virtualMachineSizeList1, List<VirtualMachineSize> virtualMachineSizeList2){
        if(virtualMachineSizeList1.toString().equals(virtualMachineSizeList2.toString()))
            return true;
        else
            return false;
    }

    @Test
    public void checkStatusOfNonExistentCluster() throws IOException, GeneralSecurityException, CloneNotSupportedException {
        AksConfig aksConfig = new AksConfig.Builder(credential, getRandomCluster(), resourceGroup, getRandomRegion(regions)).build();
        assertTrue(checkClusterStatus(aksConfig) == BaseProviderService.Status.NO_STATUS);
    }

    @Test
    public void checkStatusOfClusterWithNoConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        AksConfig aksConfig = new AksConfig.Builder(credential, getRandomCluster(), getRandomResourceGroup(), getRandomRegion(regions)).build();
        assertTrue(runClusterWithConfig(aksConfig));
        assertTrue(checkClusterStatus(aksConfig) == BaseProviderService.Status.RUNNING);
        assertTrue(deleteClusterWithConfig(aksConfig));
        assertTrue(wrapCheckClusterStatusForDelete(aksConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void checkStatusOfClusterWithExistingResourceNoConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        AksConfig aksConfig = new AksConfig.Builder(credential, getRandomCluster(), resourceGroup, getRandomRegion(regions)).setResourceType(AksConfig.ResourceType.EXISTING).build();
        assertTrue(runClusterWithConfig(aksConfig));
        assertTrue(deleteClusterWithConfig(aksConfig));
        assertTrue(checkClusterStatus(aksConfig) == BaseProviderService.Status.NO_STATUS);
    }

    @Test
    public void createClusterWithConfig() throws CloneNotSupportedException, IOException, GeneralSecurityException, InterruptedException {
        AksConfig aksConfig = new AksConfig.Builder(credential, getRandomCluster(), getRandomResourceGroup(), getRandomRegion(regions)).setNodeCount(2).setClusterVersion("1.8.11").setDiskSize(50)
                .setMachineType("STANDARD_DS1_V2").setOsType(AksConfig.OsType.LINUX).build();
        assertTrue(runClusterWithConfig(aksConfig));
        assertTrue(checkClusterStatus(aksConfig) == BaseProviderService.Status.RUNNING);
        assertTrue(deleteClusterWithConfig(aksConfig));
        assertTrue(wrapCheckClusterStatusForDelete(aksConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void createMultiThreadedDefaultNodePoolClustersWithConfig() throws IOException, GeneralSecurityException, InterruptedException, TimeoutException, ExecutionException, CloneNotSupportedException {
        List<AksConfig> aksConfigs = new ArrayList<>();
        aksConfigs.add(new AksConfig.Builder(credential, getRandomCluster(), getRandomResourceGroup(), getRandomRegion(regions)).setNodeCount(2).setClusterVersion("1.8.11").setDiskSize(50)
                .setMachineType("STANDARD_DS1_V2").setOsType(AksConfig.OsType.LINUX).build());
        aksConfigs.add(new AksConfig.Builder(credential, getRandomCluster(), getRandomResourceGroup(), getRandomRegion(regions)).setNodeCount(1).setClusterVersion("1.8.10").setDiskSize(50)
                .setMachineType("STANDARD_DS2_V2").setOsType(AksConfig.OsType.LINUX).build());
        aksConfigs.add(new AksConfig.Builder(credential, getRandomCluster(), getRandomResourceGroup(), getRandomRegion(regions)).setNodeCount(2).setClusterVersion("1.9.1").setDiskSize(50)
                .setMachineType("STANDARD_DS1_V2").setOsType(AksConfig.OsType.LINUX).build());


        List<Callable<Boolean>> callableTasks = new ArrayList<>();
        IntStream.range(0,aksConfigs.size()).forEach(index -> callableTasks.add(() -> { return runClusterWithConfig(aksConfigs.get(index));}));
        ExecutorService executorService = new ThreadPoolExecutor(2, 3, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
        List<Future<Boolean>> futures = executorService.invokeAll(callableTasks);
        assertTrue(deleteMultiThreadedClusters(futures, aksConfigs));
        Thread.sleep(veryLongSleepDuration);
    }

    @Test
    public void  checkScaleDefaultPoolNode() throws CloneNotSupportedException, IOException, InterruptedException, GeneralSecurityException {
        AksConfig aksConfig = new AksConfig.Builder(credential, getRandomCluster(), getRandomResourceGroup(), getRandomRegion(regions)).build();
        assertTrue(runClusterWithConfig(aksConfig));
        aksConfig =new AksConfig.Builder(credential, aksConfig.getClusterName(), aksConfig.getResourceGroup(), aksConfig.getRegion()).setNodeCount(2).build();
        assertTrue(scaleClusterWithConfig(aksConfig));
        assertTrue(deleteClusterWithConfig(aksConfig));
        assertTrue(wrapCheckClusterStatusForDelete(aksConfig) > 0);
        Thread.sleep(longSleepDuration);
    }
    @Test
    public void checkListMissingClustersWithResourceGroup() throws CloneNotSupportedException, IOException, GeneralSecurityException {
        AksConfig aksConfig = new AksConfig.Builder(credential, "", resourceGroup, "").build();
        assertTrue(listClustersWithConfig(aksConfig).size() == 0);
    }


    @Test
    public void testAksVMListFetcher() throws IOException, ExecutionException {

        AksVMInfo aksVMInfo = new AksVMInfo(credential);
        Set<Pair<String, String>> pairWithSameConfiguration = new HashSet<Pair<String,String>>();
        List<VirtualMachineSize>[] arrOfVirtualMachineSizeList = new List[regions.size()];
        for (int index = 0; index <regions.size(); index++){
            arrOfVirtualMachineSizeList[index] = aksVMInfo.getVirtualMachineSizes(regions.get(index));
            logger.info(arrOfVirtualMachineSizeList[index].toString());
        }
        for (int index1 = 0; index1 <regions.size(); index1++){
            for( int index2= index1+1; index2< regions.size(); index2++) {
                if(compareRegionVMList(arrOfVirtualMachineSizeList[index1], arrOfVirtualMachineSizeList[index2]))
                    pairWithSameConfiguration.add(new Pair<>(regions.get(index1), regions.get(index2)));
            }
        }

        logger.info("No of VirtualMachineSizeList matching pairs: " +pairWithSameConfiguration.size() );
        Optional<Integer> numberOfCores = arrOfVirtualMachineSizeList[regions.indexOf("eastus")].stream().filter(VMList -> VMList.getName().equals("Standard_DS2_v2"))
                .map(VMList -> VMList.getNumberOfCores()).findAny();
        logger.info("Number of Virtual Cores in region eastus Standard_DS2_v2 : "+numberOfCores.get());
        for ( VirtualMachineSize virtualMachineSize : arrOfVirtualMachineSizeList[regions.indexOf("japaneast")]){
            if(virtualMachineSize.getName().equals("Standard_GS2")) {
                logger.info("Number of Virtual Cores in region japaneast Standard_GS2 : " + virtualMachineSize.getNumberOfCores());
                break;
            }
        }
        //Checking Memory footprints in KB
        logger.info("Object type: " + arrOfVirtualMachineSizeList.getClass() +
                ", size: " + ObjectSizeCalculator.getObjectSize(arrOfVirtualMachineSizeList)/1024L + " KB");
        logger.info("Object type: " + aksVMInfo.getClass() +
                ", size: " + ObjectSizeCalculator.getObjectSize(aksVMInfo)/1024L + " KB");

    }
    @Test
    public void testResourceUsages() throws IOException, ExecutionException {
        AzureResourceUsageInfo azureResourceUsages = new AzureResourceUsageInfo(credential);
        String region ="eastus";
        String vmName = "Standard_DS11-1_v2";
        vmName = "Standard_B2s";
        logger.info("Max vCPUs for Region:"+region+" "+azureResourceUsages.getMaxNodeCount(region,vmName));
    }

    private  String[] convertVMNameIntoRegex(String vmName){
        String[] regexArr = new String[2];
        regexArr[0] = StringUtils.join(vmName.replaceFirst("\\d+[ilmrt]?(-)?\\d*[ilmrt]?(_)*", "").split("_"), "").concat("Family");;
        regexArr[1] = StringUtils.join(vmName.split("_"), "");
        return regexArr;
    }

    private boolean checkNumberInRange( String source, String searchString){
        if(source.length()<5 || source.length() >7)
            return false;
        int dashIndex = source.indexOf("_");
        if(source.charAt(0) != source.charAt(dashIndex+1))
            return false;
        if(source.charAt(0) != searchString.charAt(0))
            return false;
        else{
            try {
                int minLimit = Integer.parseInt(source.substring(1,dashIndex));
                int maxLimit = Integer.parseInt(source.substring(dashIndex+2));
                int number = Integer.parseInt(searchString.substring(1));
                if(number >=minLimit && number <=maxLimit)
                    return true;
            }
            catch(Exception e){
                return false;
            }

        }
        return false;
    }

    private long getMaxNodeCount(PagedList<UsageInner> cpuUsages, VirtualMachineSizeInner vmName){
        String[] localPattern = convertVMNameIntoRegex(vmName.name());
        Optional<UsageInner> matchingVMCpuUsage = cpuUsages.stream().filter(azureUsage ->  azureUsage.name().value().matches("(?i:" +localPattern[0]+ ")")).findAny();
        UsageInner finalMatchCpuUsage = null;
        if(!matchingVMCpuUsage.isPresent()){
            System.out.println(vmName.name());
            for(UsageInner azureUsage:cpuUsages) {
                Matcher matcher1 = Pattern.compile("(.*)([A-Z]\\d+_[A-Z]\\d+)(Family)").matcher(azureUsage.name().value());
                if (!matcher1.find()) {
                    continue;
                }
                Matcher matcher2 = Pattern.compile("(.*)([A-Z]\\d+)(.*)").matcher(localPattern[1]);
                if (!matcher2.find()) {
                    continue;
                }

                if (checkNumberInRange(matcher1.group(2), matcher2.group(2))) {
                    finalMatchCpuUsage = azureUsage;
                    break;
                }
            }
        }
        else{
            finalMatchCpuUsage = matchingVMCpuUsage.get();
        }
        Optional<UsageInner>regionalCpuUsage = cpuUsages.stream().filter(azureUsage ->  azureUsage.name().value().contains("cores")).findAny();

        if(regionalCpuUsage.isPresent()){
            UsageInner totalRegionalCpuUsage = regionalCpuUsage.get();
            if(finalMatchCpuUsage!= null){
                return Math.min(finalMatchCpuUsage.limit() - finalMatchCpuUsage.currentValue(), totalRegionalCpuUsage.limit()- totalRegionalCpuUsage.currentValue())/(vmName.numberOfCores());
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    @Test
    public void testResourceUsagesAzureApi() throws IOException, ExecutionException {

        String region ="eastus";
        String vmName = "Standard_DS11-1_v2";
        vmName = "Standard_B2s";
        ApplicationTokenCredentials credentials;
        credentials = new ApplicationTokenCredentials(
                clientID, tenantId, clientSecret, AzureEnvironment.AZURE);

        ComputeManagementClientImpl client = new ComputeManagementClientImpl(credentials);
        client.withSubscriptionId(subscriptionId);
        List<VirtualMachineSizeInner> virtualMachineInnerList = client.virtualMachineSizes().list(region);
        PagedList<UsageInner> cpuUsages = client.usages().list(region);

        for(UsageInner usageInner: cpuUsages){
            System.out.println("LocalizedName: " +usageInner.name().localizedValue()+" Name: "+usageInner.name().value()+" Current Value: "+usageInner.currentValue() +" limit: "+usageInner.limit());
        }

        for (VirtualMachineSizeInner virtualMachineSizeInner : virtualMachineInnerList ){
            logger.info("Max vCPUs for VM Name: " + virtualMachineSizeInner.name() + " Cores: "+virtualMachineSizeInner.numberOfCores() +" Region: " + region + " " + getMaxNodeCount(cpuUsages,virtualMachineSizeInner));
        }

    }
    @Test
    public void testResourceUsagesForAllVM() throws IOException, ExecutionException {
        AksVMInfo aksVMInfo = new AksVMInfo(credential);
        String region = "eastus";
        List<VirtualMachineSize>[] arrOfVirtualMachineSizeList = new List[regions.size()];
        for (int index = 0; index < regions.size(); index++) {
            arrOfVirtualMachineSizeList[index] = aksVMInfo.getVirtualMachineSizes(regions.get(index));
            logger.info(arrOfVirtualMachineSizeList[index].toString());
        }
        AzureResourceUsageInfo azureResourceUsages = new AzureResourceUsageInfo(credential);
        for (VirtualMachineSize virtualMachineSize : arrOfVirtualMachineSizeList[regions.indexOf(region)]) {
            logger.info("Max vCPUs for VM Name " + virtualMachineSize.getName() + " Region: " + region + " " + azureResourceUsages.getMaxNodeCount(region,virtualMachineSize.getName()));
        }
    }

    /*
    @Test
    public void getCodesFromVmName(){
        //(.*)(_)[A-Za-z]+(\d+)-?\d*[A-za-z]+
        Pattern p = Pattern.compile("(.*)(_)[A-Za-z]+(\\d+)-?(\\d*)[A-za-z]+");
        String vmName = "Standard_B2s";
        //vmName = "Standard_DS11-1_v2";
        Matcher matcher = p.matcher(vmName);
        while (matcher.find()) {
            System.out.println("I found the text "+matcher.group()+" starting at index "+
                    matcher.start()+" and ending at index "+matcher.end());
            System.out.println(matcher.group(3));
            //System.out.println(matcher.group(4));
            if(matcher.group(4).length() == 0)
                return matcher.group(3);
            else
                return matcher.group(4);
        }
        return 1;
    }
    */
    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main("com.nirmata.provider.managedcluster.service.AksProviderServiceTest");
    }
}
