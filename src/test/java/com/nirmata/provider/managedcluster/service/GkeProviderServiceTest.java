package com.nirmata.provider.managedcluster.service;

import com.nirmata.provider.managedcluster.BaseClient;
import com.nirmata.provider.managedcluster.util.GkeConfig;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.IntStream;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@FixMethodOrder(MethodSorters.DEFAULT)
public final class GkeProviderServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(GkeProviderServiceTest.class);
    private static final String credentialFile = "src/main/resources/gke.json";
    private static final String clusterName = "cluster-1";
    private static final String projectId = "celtic-park-213518";
    private static final String zone = "us-central1-a";
    private static final String clusterVersion = "1.10.2-gke.4";
    private final static List<String> zones = Arrays.asList("us-central1-a","us-central1-b","us-central1-c","us-central1-f"
    ,"us-east1-b","us-east1-c","us-east1-d","us-west1-a","us-west1-b","us-west1-c","us-west2-a","us-west2-b","us-west2-c");
    private static final  BaseProviderService service = GkeProviderService.getInstance();
    private static final int veryShortSleepDuration = 500;
    private static final int shortSleepDuration = 20000;
    private static final int goodSleepDuration  = 120000;
    private static final int longSleepDuration = 150000;
    private static final int veryLongSleepDuration = 240000;
    private static final int maxAttempts = 10;
    private static final int minSuffixLength = 3;

    private String getRandomCluster(){
        int length = new Random().nextInt(5) +minSuffixLength;
        return "cluster-"+RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }

    private String getRandomNodePool(){
        int length = new Random().nextInt(5) +minSuffixLength;
        return "nodepool-"+RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }

    private String getRandomZone(List<String> zones){
        return zones.get(new Random().nextInt(zones.size()));
    }

    private int deleteMultiThreadedClusters(List<Future<Boolean>> futures, List<GkeConfig> GkeConfigs) throws InterruptedException, IOException, GeneralSecurityException, TimeoutException, ExecutionException, CloneNotSupportedException {
        for(int index = 0; index <futures.size(); index++) {
            //InvokeAll is synchronous so isDone always true
            if (futures.get(index).isDone()) {
                assertTrue(futures.get(index).get(veryShortSleepDuration, TimeUnit.MILLISECONDS));
            }
            else {
                Thread.sleep(veryShortSleepDuration);
            }
        }

        Thread.sleep(veryLongSleepDuration);
        int noAttempts = maxAttempts;
        while (true) {
            if (deleteClusterWithConfig(GkeConfigs.get(0))) {
                GkeConfigs.remove(0);
            }
            noAttempts--;
            if(noAttempts==0 || GkeConfigs.isEmpty())
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }

    private boolean  runClusterWithConfig(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(gkeConfig);

        try {
            return service.createCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    private boolean  deleteClusterWithConfig(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(gkeConfig);

        try {
            return service.deleteCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        }
        return false;
    }

    private boolean  autoScaleClusterWithConfig(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(gkeConfig);

        try {
            return service.autoScaleCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        }
        return false;
    }

    private boolean  updateClusterWithConfig(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(gkeConfig);

        try {
            return service.updateCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        }
        return false;
    }

    private List<String>  listClustersWithConfig(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(gkeConfig);

        try {
            return service.listClusters(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    private int wrapDeleteCluster(GkeConfig gkeConfig) throws InterruptedException, IOException, GeneralSecurityException, CloneNotSupportedException {
        int noAttempts = maxAttempts;
        while(true){
            if(deleteClusterWithConfig(gkeConfig)) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }

    private int wrapAutoScaleCluster(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        int noAttempts = maxAttempts;
        while(true){
            if(autoScaleClusterWithConfig(gkeConfig)) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }

    private int wrapUpdateCluster(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        int noAttempts = maxAttempts;
        while(true){
            if(updateClusterWithConfig(gkeConfig)) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }

    private int wrapCheckClusterStatus(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        int noAttempts = maxAttempts;
        while(true){
            logger.info("Cluster Status: "+ checkClusterStatus(gkeConfig).toString());
            if(checkClusterStatus(gkeConfig) == BaseProviderService.Status.RUNNING) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }
    private BaseProviderService.Status  checkClusterStatus(GkeConfig gkeConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseProviderService service = GkeProviderService.getInstance();
        BaseClient client =  service.getManagedK8Client(gkeConfig);
        try{
            return service.getClusterStatus(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());
        }
        return BaseProviderService.Status.NO_STATUS;
    }
    @Test
    public void checkStatusOfNonExistentCluster() throws IOException, GeneralSecurityException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones)).build();
        assertTrue(checkClusterStatus(gkeConfig) == BaseProviderService.Status.NO_STATUS);
    }
    @Test
    public void checkStatusOfExistentCluster() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones)).build();
        assertTrue(runClusterWithConfig(gkeConfig));
        assertTrue(checkClusterStatus(gkeConfig) == BaseProviderService.Status.PROVISIONING);
        Thread.sleep(goodSleepDuration);
        assertTrue(wrapCheckClusterStatus(gkeConfig) > 0);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);

        Thread.sleep(longSleepDuration);
    }
    @Test
    public void createDefaultNodePoolClusterWithNoConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones)).build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void createDefaultNodePoolClusterWithConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(1)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    //Note: Cluster is not created
    public void createDefaultNodePoolClusterWithZeroNode() throws IOException, GeneralSecurityException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(0)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build();
        assertFalse(runClusterWithConfig(gkeConfig));
    }

    @Test
    public void createMultiThreadedDefaultNodePoolClustersWithConfig() throws IOException, GeneralSecurityException, InterruptedException, TimeoutException, ExecutionException, CloneNotSupportedException {
        List<GkeConfig> gkeConfigs = new ArrayList<>();

        gkeConfigs.add(new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(2)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build());
        gkeConfigs.add( new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId,getRandomZone(zones))
                .setInitialNodeCount(1)
                .setDiskSize(90).setImageType("COS").setMachineType("n1-standard-1").build());
        gkeConfigs.add( new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(3)
                .setDiskSize(70).setImageType("COS").setMachineType("n1-standard-1").build());
        List<Callable<Boolean>> callableTasks = new ArrayList<>();
        IntStream.range(0, gkeConfigs.size()).forEach(index -> callableTasks.add(() -> { return runClusterWithConfig(gkeConfigs.get(index));}));
        ExecutorService executorService = new ThreadPoolExecutor(2, 3, 0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<Runnable>());
        List<Future<Boolean>> futures = executorService.invokeAll(callableTasks);
        assertTrue(deleteMultiThreadedClusters(futures, gkeConfigs) > 0);
        Thread.sleep(veryLongSleepDuration);
    }

    @Test
    public void createMultiThreadedMultiNodePoolClustersWithConfig() throws IOException, GeneralSecurityException, InterruptedException, TimeoutException, ExecutionException, CloneNotSupportedException {
        List<GkeConfig> gkeConfigs = new ArrayList<>();
        gkeConfigs.add(new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(0).setPoolId(getRandomNodePool())
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build());
        String[] nodePoolIds1= {getRandomNodePool(),getRandomNodePool(),getRandomNodePool()};
        boolean[] isAutoScalingEnabledArr1 = { true, false, true};
        int [] minNodeCounts = {0, 1, 0};
        int [] maxNodeCounts = {2, 3, 2};
        gkeConfigs.add( new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId,getRandomZone(zones))
                .setInitialNodeCount(1).setPoolIds(nodePoolIds1).setAutoScaling(isAutoScalingEnabledArr1)
                .setMinNodeCount(minNodeCounts).setMaxNodeCount(maxNodeCounts)
                .setDiskSize(90).setImageType("COS").setMachineType("n1-standard-1").build());
        String[] nodePoolIds2= {getRandomNodePool(),getRandomNodePool()};
        boolean [] isAutoScalingEnabledArr2 = { true, false};
        gkeConfigs.add( new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(3).setPoolIds(nodePoolIds2).setAutoScaling(isAutoScalingEnabledArr2)
                .setDiskSize(70).setImageType("COS").setMachineType("n1-standard-1").build());
        List<Callable<Boolean>> callableTasks = new ArrayList<>();
        IntStream.range(0, gkeConfigs.size()).forEach(index -> callableTasks.add(() -> { return runClusterWithConfig(gkeConfigs.get(index));}));
        ExecutorService executorService = new ThreadPoolExecutor(2, 3, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
        List<Future<Boolean>> futures = executorService.invokeAll(callableTasks);
        assertTrue(deleteMultiThreadedClusters(futures, gkeConfigs) > 0);
        Thread.sleep(veryLongSleepDuration);
    }

    @Test
    public void createSingleNodePoolClusterWithNoConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones)).setPoolId(getRandomNodePool()).build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void createSingleNodePoolClusterAutoScalableWithConfigWithZeroNodePoolNode() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones)).setPoolId("nodepool-1")
                .setInitialNodeCount(0).setAutoScaling(true).setMinNodeCount(0).setMaxNodeCount(2)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void createMultiNodePoolClusterAutoScalableWithConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        String [] poolIds = { getRandomNodePool(), getRandomNodePool(),getRandomNodePool()};
        boolean [] isAutoScalingEnabled = { true, false, true};
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setPoolIds(poolIds)
                .setInitialNodeCount(1)
                .setAutoScaling(isAutoScalingEnabled)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(longSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void createMultiNodePoolClusterWithConfig() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        String [] poolIds = {getRandomNodePool(), getRandomNodePool(),getRandomNodePool()};
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setPoolIds(poolIds)
                .setInitialNodeCount(1)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(longSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void autoScaleDefaultPoolNode() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {

        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(1)
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-2").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        gkeConfig = new GkeConfig.Builder(credentialFile, gkeConfig.getClusterName(), projectId, gkeConfig.getZone()).setInitialNodeCount(1)
                .setAutoScaling(true).build();
        assertTrue(wrapAutoScaleCluster(gkeConfig) > 0);
        Thread.sleep(veryLongSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }

    @Test
    public void autoScaleNewPoolNode() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(1).setPoolId(getRandomNodePool())
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-1").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        gkeConfig = new GkeConfig.Builder(credentialFile, gkeConfig.getClusterName(), projectId, gkeConfig.getZone()).setPoolId(gkeConfig.getPoolId())
                .setAutoScaling(true).setMinNodeCount(1).setMaxNodeCount(4).build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(veryLongSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);
    }


    @Test
    public void updateClusterNewPoolNode() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {

        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, getRandomCluster(), projectId, getRandomZone(zones))
                .setInitialNodeCount(1).setPoolId(getRandomNodePool())
                .setDiskSize(50).setImageType("COS").setMachineType("n1-standard-1").build();
        assertTrue(runClusterWithConfig(gkeConfig));
        Thread.sleep(goodSleepDuration);
        gkeConfig = new GkeConfig.Builder(credentialFile, gkeConfig.getClusterName(), projectId, gkeConfig.getZone())
                .setClusterVersion(clusterVersion).build();
        assertTrue(wrapUpdateCluster(gkeConfig) > 0);
        Thread.sleep(veryLongSleepDuration);
        assertTrue(wrapDeleteCluster(gkeConfig) > 0);
        Thread.sleep(longSleepDuration);

    }

    @Test
    public void checkListClusters() throws IOException, GeneralSecurityException, InterruptedException, CloneNotSupportedException {
        GkeConfig gkeConfig = new GkeConfig.Builder(credentialFile, "", projectId,"us-central1-a").build();
        assertTrue(listClustersWithConfig(gkeConfig).size() == 0);

    }
    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main("com.nirmata.provider.managedcluster.service.GkeProviderServiceTest");
    }
}