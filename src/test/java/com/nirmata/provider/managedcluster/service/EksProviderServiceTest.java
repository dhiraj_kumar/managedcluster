package com.nirmata.provider.managedcluster.service;

import com.nirmata.provider.managedcluster.BaseClient;
import com.nirmata.provider.managedcluster.util.EksConfig;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.IntStream;

import static junit.framework.TestCase.assertTrue;

@FixMethodOrder(MethodSorters.DEFAULT)
public final class EksProviderServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(EksProviderServiceTest.class);
    private static final String aws_access_key_id = "AKIAITPRTLMW3VKU5PEQ";
    private static final String aws_secret_access_key = "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";
    private static final EksConfig.Credential credential = new EksConfig.Credential(aws_access_key_id,aws_secret_access_key);
    private static final List<String> regions = Arrays.asList("us-east-1","us-east-2","us-west-1","us-west-2");
    private static final String clusterName = "cluster-1";
    private static final String region = "us-west-2";
    private String getRandomRegion(List<String> regions){
        return regions.get(new Random().nextInt(regions.size()));
    }
    private final  BaseProviderService service = EksProviderService.getInstance();
    private static final int veryShortSleepDuration = 500;
    private static final int shortSleepDuration = 20000;
    private static final int goodSleepDuration  = 120000;
    private static final int longSleepDuration = 150000;
    private static final int veryLongSleepDuration = 240000;
    private static final int maxAttempts = 10;
    private static final int minSuffixLength = 3;

    private String getRandomCluster(){
        int length = new Random().nextInt(5) +minSuffixLength;
        return "cluster-"+RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }
    private String getRandomNodePool(){
        int length = new Random().nextInt(5) +minSuffixLength;
        return "nodepool-"+RandomStringUtils.randomAlphanumeric(length).toLowerCase();
    }

    private BaseProviderService.Status  checkClusterStatus(EksConfig eksConfig) throws CloneNotSupportedException, GeneralSecurityException {
        try {
            //The method doesn't throw  exception until there are any networking problems.
            BaseClient client = service.getManagedK8Client(eksConfig);
            return service.getClusterStatus(client);
        }
        catch (IOException  e){
            logger.error(e.getMessage());
        }
        return BaseProviderService.Status.NO_STATUS;
    }

    private boolean  runClusterWithConfig(EksConfig eksConfig) throws IOException, CloneNotSupportedException, GeneralSecurityException {
        BaseClient client = service.getManagedK8Client(eksConfig);

        try {
            return service.createCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());
            e.printStackTrace();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
    private boolean  deleteClusterWithConfig(EksConfig eksConfig) throws IOException, GeneralSecurityException, CloneNotSupportedException {
        BaseClient client = service.getManagedK8Client(eksConfig);

        try {
            return service.deleteCluster(client);
        }
        catch (IOException e){
            logger.error(e.getMessage());

        }
        return false;
    }

    private int wrapCheckClusterStatusForDelete(EksConfig eksConfig) throws InterruptedException, CloneNotSupportedException, GeneralSecurityException {
        int noAttempts = maxAttempts;
        while(true){
            if(checkClusterStatus(eksConfig) == BaseProviderService.Status.STOPPING) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }
    private int wrapCheckClusterStatus(EksConfig eksConfig) throws InterruptedException, CloneNotSupportedException, GeneralSecurityException {
        int noAttempts = maxAttempts;
        while(true){
            logger.info("Cluster Status: "+ checkClusterStatus(eksConfig).toString());
            if(checkClusterStatus(eksConfig) == BaseProviderService.Status.RUNNING) {
                break;
            }
            noAttempts--;
            if(noAttempts==0)
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }
    private int deleteMultiThreadedClusters(List<Future<Boolean>> futures, List<EksConfig> eksConfigs) throws InterruptedException, IOException, GeneralSecurityException, TimeoutException, ExecutionException, CloneNotSupportedException {
        for(int index = 0; index <futures.size(); index++) {
            //InvokeAll is synchronous so isDone always true
            if (futures.get(index).isDone()) {
                assertTrue(futures.get(index).get(veryShortSleepDuration, TimeUnit.MILLISECONDS));
            }
            else {
                Thread.sleep(veryShortSleepDuration);
            }
        }

        Thread.sleep(veryLongSleepDuration);
        int noAttempts = maxAttempts;
        while (true) {
            if (deleteClusterWithConfig(eksConfigs.get(0))) {
                eksConfigs.remove(0);
            }
            noAttempts--;
            if(noAttempts==0 || eksConfigs.isEmpty())
                break;
            Thread.sleep(shortSleepDuration);
        }
        return noAttempts;
    }
    @Test
    public void checkStatusOfNonExistentCluster() throws IOException, GeneralSecurityException, CloneNotSupportedException {
        EksConfig eksConfig = new EksConfig.Builder(credential, getRandomCluster(),  region).build();
        assertTrue(checkClusterStatus(eksConfig) == BaseProviderService.Status.NO_STATUS);
    }

    @Test
    public void checkStatusOfClusterWithNoConfig() throws IOException, GeneralSecurityException, CloneNotSupportedException, InterruptedException {
        EksConfig eksConfig = new EksConfig.Builder(credential, getRandomCluster(),  region).build();
        assertTrue(runClusterWithConfig(eksConfig));
        assertTrue(checkClusterStatus(eksConfig) == BaseProviderService.Status.PROVISIONING);
        Thread.sleep(veryLongSleepDuration);
        Thread.sleep(longSleepDuration);
        assertTrue(wrapCheckClusterStatus(eksConfig) > 0);
        assertTrue(deleteClusterWithConfig(eksConfig));
    }

    @Test
    public void checkCreateClusterWithConfigDefaultVpc() throws IOException, GeneralSecurityException, CloneNotSupportedException, InterruptedException {
        EksConfig eksConfig = new EksConfig.Builder(credential,clusterName, region).setVpcType(EksConfig.VpcType.DEFAULT).build();
        assertTrue(runClusterWithConfig(eksConfig));
        Thread.sleep(veryLongSleepDuration);
        Thread.sleep(longSleepDuration);
        assertTrue(wrapCheckClusterStatus(eksConfig) > 0);
        assertTrue(deleteClusterWithConfig(eksConfig));
        assertTrue(wrapCheckClusterStatusForDelete(eksConfig) > 0);
        Thread.sleep(longSleepDuration);
    }
    @Test
    public void createMultiThreadedMultiNodePoolClustersWithConfig() throws CloneNotSupportedException, InterruptedException, GeneralSecurityException, TimeoutException, ExecutionException, IOException {
        List<EksConfig> eksConfigs = new ArrayList<>();
        int [] minNodeCounts = {1, 1, 1};
        int [] maxNodeCounts = {1, 3, 2};
        String[] nodePoolIds1= {getRandomNodePool(),getRandomNodePool(),getRandomNodePool()};
        eksConfigs.add( new EksConfig.Builder(credential, getRandomCluster(), region)
                .setMinNodeCount(minNodeCounts).setMaxNodeCount(maxNodeCounts).setPoolIds(nodePoolIds1).build());
        String[] nodePoolIds2= {getRandomNodePool(),getRandomNodePool()};
        eksConfigs.add( new EksConfig.Builder(credential, getRandomCluster(), region)
                .setMinNodeCount(minNodeCounts).setMaxNodeCount(maxNodeCounts).setPoolIds(nodePoolIds2).build());
        List<Callable<Boolean>> callableTasks = new ArrayList<>();

        IntStream.range(0, eksConfigs.size()).forEach(index -> callableTasks.add(() -> { return runClusterWithConfig(eksConfigs.get(index));}));
        ExecutorService executorService = new ThreadPoolExecutor(2, 3, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
        List<Future<Boolean>> futures = executorService.invokeAll(callableTasks);
        assertTrue(deleteMultiThreadedClusters(futures, eksConfigs) > 0);
        Thread.sleep(longSleepDuration);
        Thread.sleep(veryLongSleepDuration);

    }

}
