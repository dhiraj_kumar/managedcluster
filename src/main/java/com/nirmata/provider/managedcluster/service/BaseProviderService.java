package com.nirmata.provider.managedcluster.service;

import com.nirmata.provider.managedcluster.*;
import com.nirmata.provider.managedcluster.util.AksConfig;
import com.nirmata.provider.managedcluster.util.Config;
import com.nirmata.provider.managedcluster.util.EksConfig;
import com.nirmata.provider.managedcluster.util.GkeConfig;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.List;


public interface BaseProviderService {
     enum Status{
          NO_STATUS,PROVISIONING, RUNNING, RUNNING_ERROR, UPDATING, STOPPING, OTHERS, ERROR;
     }
     Status getClusterStatus(BaseClient client) throws IOException;

     Status getNodePoolStatus(BaseClient client) throws IOException;

     boolean createCluster(BaseClient client) throws Exception;

     boolean scaleCluster(BaseClient client) throws IOException;

     boolean autoScaleCluster(BaseClient client) throws IOException;

     boolean updateCluster(BaseClient client) throws IOException;

     boolean  deleteCluster(BaseClient client) throws IOException;

     List<String> listClusters(BaseClient client) throws IOException;

     default  BaseClient getManagedK8Client(Config config) throws CloneNotSupportedException, IOException, GeneralSecurityException {
         Config.ProviderType providerType = config.getProviderType();
         switch (providerType){
             case AKS:
                 return new AksClient((AksConfig)config);
             case GKE:
                 return new GkeClient((GkeConfig)config);
             case EKS:
                 return new EksClient((EksConfig) config);
         }
         return null;
     }
}
