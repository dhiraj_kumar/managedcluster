package com.nirmata.provider.managedcluster.service;

import com.google.api.services.container.Container;
import com.google.api.services.container.Container.Projects;
import com.google.api.services.container.model.*;
import com.nirmata.provider.managedcluster.*;
import com.nirmata.provider.managedcluster.util.*;
import java.io.IOException;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GkeProviderService implements BaseProviderService, Serializable,Cloneable {
    private static final Logger logger = LoggerFactory.getLogger(GkeProviderService.class);
    private static final GkeProviderService instance = new GkeProviderService();

    @Override
    public Status getClusterStatus(BaseClient baseClient) throws IOException {
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        Container.Projects.Zones.Clusters.Get request = ((GkeClient)baseClient).getContainer().projects().zones().clusters()
                        .get(gkeConfig.getProjectId(), gkeConfig.getZone(),gkeConfig.getClusterName());
        //Method Throws an Exception if cluster doesn't exist
        Cluster cluster = request.execute();

        long provisioningCount = cluster.getNodePools().stream().map(NodePool::getStatus).filter(status -> !status.equalsIgnoreCase("running")).count();
        String clusterStatus = cluster.getStatus();
        if(clusterStatus.equalsIgnoreCase("provisioning") || provisioningCount > 0){
            return Status.PROVISIONING;
        }
        else if(clusterStatus.equalsIgnoreCase("running"))
            return Status.RUNNING;
        else if(clusterStatus.equalsIgnoreCase("reconciling"))
            return Status.UPDATING;
        else if(clusterStatus.equalsIgnoreCase("error"))
            return Status.ERROR;
        else if(clusterStatus.equalsIgnoreCase("stopping"))
            return Status.STOPPING;
        else
            return Status.OTHERS;
    }

    @Override
    public Status getNodePoolStatus(BaseClient baseClient) throws IOException {
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        Container.Projects.Zones.Clusters.NodePools.Get request = ((GkeClient)baseClient).getContainer().projects().zones().clusters().nodePools()
                .get(gkeConfig.getProjectId(), gkeConfig.getZone(),gkeConfig.getClusterName(), gkeConfig.getPoolId());
        //Method Throws an Exception if nodepool doesn't exist
        NodePool nodePool = request.execute();
        String poolStatus = nodePool.getStatus();
        if(poolStatus.equalsIgnoreCase("provisioning"))
            return Status.PROVISIONING;
        else if(poolStatus.equalsIgnoreCase("running"))
            return Status.RUNNING;
        else if (poolStatus.equalsIgnoreCase("running_with_error"))
            return Status.RUNNING_ERROR;
        else if(poolStatus.equalsIgnoreCase("reconciling"))
            return Status.UPDATING;
        else if(poolStatus.equalsIgnoreCase("error"))
            return Status.ERROR;
        else if(poolStatus.equalsIgnoreCase("stopping"))
            return Status.STOPPING;
        else
            return Status.OTHERS;
    }

    @Override
     public boolean createCluster(BaseClient baseClient) throws IOException{
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        CreateClusterRequest requestBody = new CreateClusterRequest();
        Cluster cluster = new Cluster().setName(gkeConfig.getClusterName()).setZone(gkeConfig.getZone());

        if(gkeConfig.getPoolType() == GkeConfig.PoolType.DEFAULT){
            //Autoscaling the default pool is not supported at creation of cluster
            cluster = cluster.setInitialNodeCount(gkeConfig.getInitialNodeCount()).setNodeConfig(gkeConfig.getNodeConfig());
        }
        else{
            List<NodePool> nodePools = new ArrayList<>();
            NodePool nodePool;
            if(gkeConfig.getNoPools() == 1) {
                nodePool = new NodePool().setName(gkeConfig.getPoolId()).setInitialNodeCount(gkeConfig.getInitialNodeCount())
                        .setManagement(new NodeManagement().setAutoUpgrade(true)).setConfig(gkeConfig.getNodeConfig());
                if(gkeConfig.isAutoScalingEnabled())
                    nodePool.setAutoscaling(new NodePoolAutoscaling().setEnabled(true).setMinNodeCount(gkeConfig.getMinNodeCount())
                            .setMaxNodeCount(gkeConfig.getMaxNodeCount()));
                nodePools.add(nodePool);
            }
            else{
                for(int poolNo = 0; poolNo < gkeConfig.getNoPools(); poolNo++) {
                    nodePool = new NodePool().setName(gkeConfig.getPoolIds().get(poolNo)).setInitialNodeCount(gkeConfig.getInitialNodeCounts()[poolNo])
                            .setManagement(new NodeManagement().setAutoUpgrade(true)).setConfig(gkeConfig.getNodeConfigs()[poolNo]);
                    if (gkeConfig.getIsAutoScalingEnabledArr()[poolNo]) {
                        nodePool.setAutoscaling(new NodePoolAutoscaling().setEnabled(true).setMinNodeCount(gkeConfig.getMinNodeCounts()[poolNo])
                                .setMaxNodeCount(gkeConfig.getMaxNodeCounts()[poolNo]));
                    }
                    nodePools.add(nodePool);
                }
            }
            cluster.setNodePools(nodePools);
        }
        if(gkeConfig.getClusterVersion()!= null)
            cluster = cluster.setCurrentMasterVersion(gkeConfig.getClusterVersion());
        if(gkeConfig.getNodeVersion()!= null)
            cluster = cluster.setCurrentNodeVersion(gkeConfig.getNodeVersion());
        if(gkeConfig.getIsPrivateClusterEnabled() || gkeConfig.getIsVpcEnabled()) {
            cluster = cluster.setIpAllocationPolicy(new IPAllocationPolicy().setUseIpAliases(true).setCreateSubnetwork(true));
        }
        //Nodes don't have ip address in private cluster. masterIpv4CidrBlock is used instead.
        if(gkeConfig.getIsPrivateClusterEnabled()){
            // Beta version  of GCloud, Console supports  privateCluster
            //Note: To be Supported Soon
            //cluster = cluster.setMasterIpv4CidrBlock("")
            //cluster = cluster.setPrivateCluster(true);
        }
        requestBody.set("cluster",cluster);
        Projects.Zones.Clusters.Create request =
                ((GkeClient)baseClient).getContainer().projects().zones().clusters().create(gkeConfig.getProjectId(), gkeConfig.getZone(), requestBody);
        Operation response = request.execute();
        return true;
    }
    
    @Override
    public boolean autoScaleCluster(BaseClient baseClient) throws IOException{
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        SetNodePoolAutoscalingRequest requestBody = new SetNodePoolAutoscalingRequest().setAutoscaling(new NodePoolAutoscaling().setEnabled(gkeConfig.isAutoScalingEnabled()).setMinNodeCount(gkeConfig.getMinNodeCount())
                .setMaxNodeCount(gkeConfig.getMaxNodeCount()));
        Container.Projects.Zones.Clusters.NodePools.Autoscaling request =
                ((GkeClient)baseClient).getContainer()
                        .projects()
                        .zones()
                        .clusters()
                        .nodePools()
                        .autoscaling(gkeConfig.getProjectId(), gkeConfig.getZone(), gkeConfig.getClusterName(), gkeConfig.getPoolId(), requestBody);

        Operation response = request.execute();
        return true;

    }

    @Override
    public boolean scaleCluster(BaseClient baseClient) throws IOException {
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        SetNodePoolSizeRequest requestBody = new SetNodePoolSizeRequest().setNodeCount(gkeConfig.getInitialNodeCount());

        Container.Projects.Zones.Clusters.NodePools.SetSize request =
                ((GkeClient)baseClient).getContainer()
                        .projects()
                        .zones()
                        .clusters()
                        .nodePools()
                        .setSize(gkeConfig.getProjectId(), gkeConfig.getZone(), gkeConfig.getClusterName(), gkeConfig.getPoolId(), requestBody);

        Operation response = request.execute();
        return true;
    }

    @Override
    public boolean updateCluster(BaseClient baseClient) throws IOException{
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        ClusterUpdate clusterUpdate = new ClusterUpdate();
        if(gkeConfig.getClusterVersion()!= null)
            clusterUpdate = clusterUpdate.setDesiredMasterVersion(gkeConfig.getClusterVersion());
        if(gkeConfig.getNodeVersion()!= null)
            clusterUpdate = clusterUpdate.setDesiredNodeVersion(gkeConfig.getNodeVersion());
        UpdateClusterRequest requestBody = new UpdateClusterRequest().setUpdate(clusterUpdate);
        Container.Projects.Zones.Clusters.Update request = ((GkeClient)baseClient).getContainer()
                        .projects()
                        .zones()
                        .clusters()
                        .update(gkeConfig.getProjectId(), gkeConfig.getZone(), gkeConfig.getClusterName(), requestBody);

        Operation response = request.execute();

        return true;
    }

    @Override
    public boolean deleteCluster(BaseClient baseClient) throws IOException {
        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        Container.Projects.Zones.Clusters.Delete request =
                ((GkeClient)baseClient).getContainer().projects().zones().clusters().delete(gkeConfig.getProjectId(), gkeConfig.getZone(), gkeConfig.getClusterName());
        Operation response = request.execute();
        return true;
    }

    @Override
    public List<String> listClusters(BaseClient baseClient) throws IOException {

        GkeConfig gkeConfig = ((GkeClient)baseClient).getConfig();
        Container.Projects.Zones.Clusters.List request =
                ((GkeClient)baseClient).getContainer().projects().zones().clusters().list(gkeConfig.getProjectId(), gkeConfig.getZone());
        ListClustersResponse response = request.execute();
        if(response.size() > 0)
            return response.getClusters().stream().map(Cluster::getName).collect(Collectors.toList());
        else
            return new ArrayList<>();
    }

    public static GkeProviderService getInstance(){

        return instance;
    }


    private GkeProviderService() {

    }
}
