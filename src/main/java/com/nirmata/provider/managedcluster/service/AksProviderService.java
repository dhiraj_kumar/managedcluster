package com.nirmata.provider.managedcluster.service;

import com.microsoft.azure.PagedList;
import com.microsoft.azure.management.containerservice.*;
import com.microsoft.azure.management.containerservice.implementation.ManagedClusterUpgradeProfileInner;
import com.nirmata.provider.managedcluster.AksClient;
import com.nirmata.provider.managedcluster.util.AksConfig;
import com.nirmata.provider.managedcluster.util.SSHShell;
import com.nirmata.provider.managedcluster.BaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class AksProviderService implements BaseProviderService, Serializable, Cloneable{
    private static final Logger logger = LoggerFactory.getLogger(AksProviderService.class);
    private static final AksProviderService instance = new AksProviderService();

    @Override
    public Status getClusterStatus(BaseClient baseClient) {
        AksConfig aksConfig = ((AksClient)baseClient).getConfig();
        KubernetesCluster kubernetesCluster =  ((AksClient)baseClient).getAzure().kubernetesClusters().getByResourceGroup(aksConfig.getResourceGroup(),aksConfig.getClusterName());
        /*
        The status changes to "creating" after KubernetesCluster object is created;
        Poll getClusterStatus few  number of times if we switch to async create,scale apis; ensures thread is alive until server processes the httpClient
        */
        if( kubernetesCluster!= null){
            String clusterStatus = kubernetesCluster.provisioningState();
            if(clusterStatus.equalsIgnoreCase("creating") || clusterStatus.equalsIgnoreCase("provisioning"))
                return Status.PROVISIONING;
            else if(clusterStatus.equalsIgnoreCase("succeeded"))
                return Status.RUNNING;
            else if(clusterStatus.equalsIgnoreCase("updating"))
                return Status.UPDATING;
            else if(clusterStatus.equalsIgnoreCase("deleting"))
                return Status.STOPPING;
            else if(clusterStatus.equalsIgnoreCase("failed"))
                return Status.ERROR;
            else
                return Status.OTHERS;
        }
        /*Cluster will be null under two scenario
         1. Server hasn't yet processed the request.
         2. Cluster doesn't exist.
        */
        return Status.NO_STATUS;
    }

    @Override
    public Status getNodePoolStatus(BaseClient client) throws IOException {
        return Status.RUNNING;
    }

    @Override
    public boolean createCluster(BaseClient baseClient) throws Exception {
        AksConfig aksConfig = ((AksClient)baseClient).getConfig();
        final String defaultRootUserName = "aksuser";
        final String defaultPoolName = "agentpool";
        final String defaultDnsPrefix = "dns-" + aksConfig.getClusterName();
        SSHShell.SshPublicPrivateKey sshKeys = SSHShell.generateSSHKeys("", "ACS");

        KubernetesCluster.DefinitionStages.WithGroup withRegion =   ((AksClient)baseClient).getAzure().kubernetesClusters().define(aksConfig.getClusterName())
               .withRegion(aksConfig.getRegion());
       KubernetesCluster.DefinitionStages.WithVersion withVersion;
        if(aksConfig.getResourceType() == AksConfig.ResourceType.EXISTING)
            withVersion = withRegion.withExistingResourceGroup(aksConfig.getResourceGroup());
        else
            withVersion = withRegion.withNewResourceGroup(aksConfig.getResourceGroup());
        KubernetesCluster.DefinitionStages.WithLinuxRootUsername withLinuxRootUsername;
        if(aksConfig.getClusterVersion()!= null)
            withLinuxRootUsername = withVersion.withVersion(aksConfig.getClusterVersion());
        else
            withLinuxRootUsername = withVersion.withLatestVersion();
        KubernetesClusterAgentPool.DefinitionStages.WithAttach<KubernetesCluster.DefinitionStages.WithCreate>  withCreateBlank =
        withLinuxRootUsername.withRootUsername(defaultRootUserName)
                .withSshKey(sshKeys.getSshPublicKey())
                .withServicePrincipalClientId(aksConfig.getCredential().getClientID())
                .withServicePrincipalSecret(aksConfig.getCredential().getClientSecret())
                .defineAgentPool(defaultPoolName)
                .withVirtualMachineCount(aksConfig.getNodeCount())
                .withVirtualMachineSize(ContainerServiceVMSizeTypes.fromString(aksConfig.getMachineType()))
                .withOSType(OSType.fromString(aksConfig.getOsType().toString()));

        if(aksConfig.getDiskSize() > 0) {
            withCreateBlank =  withCreateBlank.withOSDiskSizeInGB(aksConfig.getDiskSize());
        }
        KubernetesCluster kubernetesCluster = withCreateBlank
                .attach()
                .withDnsPrefix(defaultDnsPrefix)
                .create();
        return true;
    }

    @Override
    public boolean scaleCluster(BaseClient baseClient) {
        AksConfig aksConfig = ((AksClient)baseClient).getConfig();
        KubernetesCluster kubernetesCluster =  ((AksClient)baseClient).getAzure().kubernetesClusters().getByResourceGroup(aksConfig.getResourceGroup(),aksConfig.getClusterName());
        kubernetesCluster.update().withAgentVirtualMachineCount(aksConfig.getNodeCount()).apply();
        return true;
    }

    @Override
    public boolean autoScaleCluster(BaseClient client) {
       return scaleCluster(client);
    }

    @Override
    public boolean updateCluster(BaseClient baseClient) {
        //Not Supported Yet
        AksConfig aksconfig = ((AksClient)baseClient).getConfig();
        KubernetesCluster kubernetesCluster =  ((AksClient)baseClient).getAzure().kubernetesClusters().getByResourceGroup(aksconfig.getResourceGroup(),aksconfig.getClusterName());
        KubernetesClusterUpgradeProfile kubernetesClusterUpgradeProfile = new KubernetesClusterUpgradeProfile() {
           final ManagedClusterUpgradeProfileInner inner = new ManagedClusterUpgradeProfileInner();
            @Override
            public String id() {
                return inner.id();
            }

            @Override
            public String name() {
                return inner.name();
            }

            @Override
            public String type() {
                return inner.type();
            }
            @Override
            public ManagedClusterUpgradeProfileInner inner() {
                return inner.withControlPlaneProfile(new ManagedClusterPoolUpgradeProfile()
                .withKubernetesVersion(aksconfig.getClusterVersion()).withName(kubernetesCluster.agentPools().get(0).name())
                        .withOsType(OSType.fromString(aksconfig.getOsType().toString())));
            }

        };
        return false;
    }

    @Override
    public boolean deleteCluster(BaseClient baseClient) {
        AksConfig aksConfig = ((AksClient)baseClient).getConfig();
        KubernetesCluster kubernetesCluster =  ((AksClient)baseClient).getAzure().kubernetesClusters().getByResourceGroup(aksConfig.getResourceGroup(),aksConfig.getClusterName());
        if(aksConfig.getResourceType() == AksConfig.ResourceType.EXISTING)
            //sync
            ((AksClient) baseClient).getAzure().kubernetesClusters().deleteByResourceGroup(aksConfig.getResourceGroup(), aksConfig.getClusterName());
        else
            //async. Takes few milli seconds before the status of object is changed to deleting.
            ((AksClient) baseClient).getAzure().resourceGroups().beginDeleteByName(aksConfig.getResourceGroup());
        return true;
    }

    @Override
    public List<String> listClusters(BaseClient client){
        AksConfig aksconfig = ((AksClient)client).getConfig();
        KubernetesClusters kubernetesClusters = ((AksClient) client).getAzure().kubernetesClusters();
        PagedList<KubernetesCluster> pagedList = kubernetesClusters.listByResourceGroup(aksconfig.getResourceGroup());
        if(pagedList.size() > 0)
            return pagedList.stream().map(cluster -> cluster.name()).collect(Collectors.toList());
        else {
            return new ArrayList<>();
        }
    }

    public static AksProviderService getInstance(){

        return instance;
    }


    private AksProviderService() {

    }

}
