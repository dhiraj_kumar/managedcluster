package com.nirmata.provider.managedcluster.service;
import com.amazonaws.services.cloudformation.model.*;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.eks.model.*;
import com.amazonaws.services.identitymanagement.model.*;
import com.nirmata.provider.managedcluster.BaseClient;
import com.nirmata.provider.managedcluster.EksClient;
import com.nirmata.provider.managedcluster.util.EKsRoleConfig;
import com.nirmata.provider.managedcluster.util.IPv4CIDRBlock;
import com.nirmata.provider.managedcluster.util.EksConfig;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class EksProviderService implements BaseProviderService, Serializable,Cloneable{
    private static final Logger logger = LoggerFactory.getLogger(EksProviderService.class);
    private static final EksProviderService instance = new EksProviderService();

    private List<String> getAvailableZones( BaseClient baseClient){
        DescribeAvailabilityZonesResult response = ((EksClient) baseClient).getEc2().describeAvailabilityZones(new DescribeAvailabilityZonesRequest());
        return response.getAvailabilityZones().parallelStream().map(AvailabilityZone::getZoneName).collect(Collectors.toList());
    }
    private Pair<String,List<String>> getVpcSubnet(BaseClient baseClient, EksConfig eksConfig){
        Vpc vpc = new Vpc();

        //Need at least 2 subnet from different zones. Below copy operation requires 3 mininal subnets
        List<String> zones = getAvailableZones(baseClient).stream()
                .flatMap(zone -> Stream.generate(() -> zone).limit(2))
                .collect(Collectors.toList());
        List<String> subnets  = new ArrayList<>();
        if(eksConfig.getVpcType() == EksConfig.VpcType.DEFAULT){
            //Check if Default VPC exists for the given region and create it if doesn't.
            Optional<Vpc> vpcOptional= ((EksClient) baseClient).getEc2().describeVpcs().getVpcs().stream().filter(Vpc::getIsDefault).findAny();
            if(vpcOptional.isPresent()){
                //Fetch the default VPC subnet
                vpc = vpcOptional.get();
                Vpc finalVpc = vpc;
                subnets = ((EksClient) baseClient).getEc2().describeSubnets().getSubnets().stream().filter(ele -> ele.getVpcId().equals(finalVpc.getVpcId()))
                        .map(Subnet::getSubnetId).collect(Collectors.toList());
            }
            else{
                //If user have deleted the Default VPC by mistake.
                CreateDefaultVpcRequest createDefaultVpcRequest = new CreateDefaultVpcRequest();
                CreateDefaultVpcResult createdefaultVpcResult = ((EksClient) baseClient).getEc2().createDefaultVpc(createDefaultVpcRequest);
                logger.info(createdefaultVpcResult.toString());
                vpc = createdefaultVpcResult.getVpc();
                //Default subnets are created automatically;
                Vpc finalVpc1 = vpc;
                subnets = ((EksClient) baseClient).getEc2().describeSubnets().getSubnets().stream().filter(ele -> ele.getVpcId().equals(finalVpc1.getVpcId()))
                        .map(Subnet::getSubnetId).collect(Collectors.toList());
            }
            return new Pair<>(vpc.getVpcId(),subnets);
        }
        else {
            //Create  a new VPC for every cluster
            CreateVpcRequest createVpcRequest = new CreateVpcRequest();
            int maxAttempts = 5;
            while (maxAttempts > 0){
                Stack<String> stackZones = new Stack<>();
                stackZones.addAll(zones);
                String vpcName = IPv4CIDRBlock.getCIDRVpc();
                createVpcRequest.setCidrBlock(vpcName);
                //Collision of VPC name allowed for single user. This will never fail
                CreateVpcResult createVpcResult = ((EksClient) baseClient).getEc2().createVpc(createVpcRequest);
                vpc = createVpcResult.getVpc();
                //For 20 bit subnet with 16 bit vpc.
                final int upperLimit = (1 << 4);
                Set<Integer> generatedIds = new HashSet<Integer>();
                //All subnets for the user account
                Set<String> storedSubnets = ((EksClient) baseClient).getEc2().describeSubnets().getSubnets().parallelStream().map(Subnet::getCidrBlock).collect(Collectors.toSet());
                final int minNoSubnet = 3;
                final int maxNoSubnet = 5;
                List<String> subnetNames = new ArrayList<>();
                Random random = new Random();
                // Handling Collision of many subnet for an user account with a single thread
                while (generatedIds.size() < maxNoSubnet)
                {
                    Integer next = random.nextInt(upperLimit);
                    if(generatedIds.add(next)){
                        String subnetName = IPv4CIDRBlock.getCIDRSubnet(vpcName, next);
                        if(storedSubnets.contains(subnetName)) {
                            generatedIds.remove(next);
                        }
                        else{
                            subnetNames.add(subnetName);
                        }
                    }
                }
                List<CreateSubnetRequest> createSubnetRequests = subnetNames.stream().map(subNet -> new CreateSubnetRequest()
                        .withCidrBlock(subNet)
                        .withAvailabilityZone(stackZones.pop())
                        .withVpcId(createVpcResult.getVpc().getVpcId()))
                        .collect(Collectors.toList());
                List<CreateSubnetResult> createSubnetResults = new ArrayList<>();

                /*Handling collision for multi threaded login. It will throw exception if  simultaneous threads of same user, find the  same subnet
                and  Probability of Collision for two random Subnet is 1/ 2^12 = 0.000244
                */
                try {
                    createSubnetRequests.forEach(request -> createSubnetResults.add(((EksClient) baseClient).getEc2().createSubnet(request)));
                    subnets = createSubnetResults.stream().map(subnetResult -> subnetResult.getSubnet().getSubnetId()).collect(Collectors.toList());
                    break;
                } catch (com.amazonaws.services.ec2.model.AmazonEC2Exception e) {
                    logger.warn(e.getMessage());
                    if(createSubnetResults.size() < minNoSubnet) {
                        createSubnetResults.forEach(createSubnetResult -> {
                            ((EksClient) baseClient).getEc2().deleteSubnet(new DeleteSubnetRequest().withSubnetId(createSubnetResult.getSubnet().getSubnetId()));
                        });
                        ((EksClient) baseClient).getEc2().deleteVpc(new DeleteVpcRequest().withVpcId(createVpcResult.getVpc().getVpcId()));
                        maxAttempts--;
                    }
                    else{
                        subnets = createSubnetResults.stream().map(subnetResult -> subnetResult.getSubnet().getSubnetId()).collect(Collectors.toList());
                        break;
                    }
                }
            }

            //Statistically, it will rarely happen;
            if(maxAttempts == 0) {
                return new Pair<>(null,null);
            }
            return new Pair<>(vpc.getVpcId(),subnets);
        }

    }

    private Role getRole(BaseClient baseClient, EksConfig eksConfig) throws InterruptedException {
        ListRolesRequest listRolesRequest = new ListRolesRequest();
        ListRolesResult listRolesResult =((EksClient) baseClient).getAmazonIdentityManagement().listRoles(listRolesRequest);
        Optional<Role> roleOptional= listRolesResult.getRoles().stream().filter(roleName ->roleName.getDescription().contains("Allows EKS to manage clusters")).findAny();
        if(roleOptional.isPresent())
            return roleOptional.get();
        else{
            //Create Role
            CreateRoleRequest createRoleRequest = new CreateRoleRequest().withRoleName(EKsRoleConfig.getRoleName())
                    .withDescription(EKsRoleConfig.getRoleDescription()).withAssumeRolePolicyDocument(EKsRoleConfig.getRolePolicyDoc());
            CreateRoleResult createRoleResult = ((EksClient) baseClient).getAmazonIdentityManagement().createRole(createRoleRequest);
            GetPolicyRequest clusterPolicyRequest = new GetPolicyRequest().withPolicyArn(EKsRoleConfig.getEksClusterPolicyArn());
            GetPolicyRequest servicePolicyRequest = new GetPolicyRequest().withPolicyArn(EKsRoleConfig.getEksServicePolicyArn());

            ((EksClient) baseClient).getAmazonIdentityManagement().attachRolePolicy(new AttachRolePolicyRequest().withRoleName(EKsRoleConfig.getRoleName())
                    .withPolicyArn(clusterPolicyRequest.getPolicyArn()));
            ((EksClient) baseClient).getAmazonIdentityManagement().attachRolePolicy(new AttachRolePolicyRequest().withRoleName(EKsRoleConfig.getRoleName())
                    .withPolicyArn(servicePolicyRequest.getPolicyArn()));
            //This will be called once
            Thread.sleep(30000);
            if(createRoleResult!= null)
                return createRoleResult.getRole();
            else
                return null;
        }
    }
    //Key pair is created for every region
    private String getSshKeyPair(BaseClient baseClient, EksConfig eksConfig){
        String keyName = EKsRoleConfig.getSshKeyName()+"_"+eksConfig.getRegion();
        Optional<String> optionalKey = ((EksClient) baseClient).getEc2().describeKeyPairs().getKeyPairs().stream()
                .map(KeyPairInfo::getKeyName).filter(name -> name.equalsIgnoreCase(keyName)).findAny();
        if(optionalKey.isPresent()) {
            return optionalKey.get();
        }
        else{
            CreateKeyPairRequest createKeyPairRequest = new CreateKeyPairRequest().withKeyName(keyName);
            CreateKeyPairResult createKeyPairResult = ((EksClient) baseClient).getEc2().createKeyPair(createKeyPairRequest);
            return createKeyPairResult.getKeyPair().getKeyName();
        }

    }

    private void createNodePoolsOnEc2(BaseClient baseClient, EksConfig eksConfig, List<String> securityGroups, String keyName,
                                     Pair<String,List<String>> vpcSubnets){

        if(eksConfig.getNoPools() == 1){
            List<Parameter> nodePoolParameters = eksConfig.getNodePoolParameters();
            CreateStackRequest workerNodeStackRequest = new CreateStackRequest();
            workerNodeStackRequest.setTemplateURL(EKsRoleConfig.getNodePoolTemplate());
            String workerNodeStackName = eksConfig.getPoolId();
            nodePoolParameters.get(0).setParameterValue(eksConfig.getClusterName());
            nodePoolParameters.get(1).setParameterValue(String.join(",",securityGroups));
            nodePoolParameters.get(2).setParameterValue(eksConfig.getPoolId());
            nodePoolParameters.get(3).setParameterValue(String.valueOf(eksConfig.getMinNodeCount()));
            nodePoolParameters.get(4).setParameterValue(String.valueOf(eksConfig.getMaxNodeCount()));
            nodePoolParameters.get(5).setParameterValue(eksConfig.getMachineType());
            nodePoolParameters.get(6).setParameterValue((eksConfig.getAmiType()));
            nodePoolParameters.get(7).setParameterValue(keyName);
            nodePoolParameters.get(8).setParameterValue(vpcSubnets.getKey());
            nodePoolParameters.get(9).setParameterValue(String.join(",",vpcSubnets.getValue()));
            workerNodeStackRequest.withStackName(workerNodeStackName).setParameters(nodePoolParameters);
            workerNodeStackRequest.setCapabilities(Collections.singletonList("CAPABILITY_IAM"));
            //This will never fail until you put in wrong parameters. Ec2 provisioning can be checked with getNodePoolStatus
            CreateStackResult resultStack = ((EksClient) baseClient).getAmazonCloudFormation().createStack(workerNodeStackRequest);
        }
        else {
            for(int index = 0; index< eksConfig.getNoPools(); index++){
                // clone for every node pool
                List<Parameter> nodePoolParameters = eksConfig.getNodePoolParameters();
                CreateStackRequest workerNodeStackRequest = new CreateStackRequest();
                workerNodeStackRequest.setTemplateURL(EKsRoleConfig.getNodePoolTemplate());
                String workerNodeStackName = eksConfig.getPoolIds().get(index);
                nodePoolParameters.get(0).setParameterValue(eksConfig.getClusterName());
                nodePoolParameters.get(1).setParameterValue(String.join(",",securityGroups));
                nodePoolParameters.get(2).setParameterValue(eksConfig.getPoolIds().get(index));
                nodePoolParameters.get(3).setParameterValue(String.valueOf(eksConfig.getMinNodeCounts()[index]));
                nodePoolParameters.get(4).setParameterValue(String.valueOf(eksConfig.getMaxNodeCounts()[index]));
                nodePoolParameters.get(5).setParameterValue(eksConfig.getMachineTypes().get(index));
                nodePoolParameters.get(6).setParameterValue((eksConfig.getAmiTypes().get(index)));
                nodePoolParameters.get(7).setParameterValue(keyName);
                nodePoolParameters.get(8).setParameterValue(vpcSubnets.getKey());
                nodePoolParameters.get(9).setParameterValue(String.join(",",vpcSubnets.getValue()));
                workerNodeStackRequest.withStackName(workerNodeStackName).setParameters(nodePoolParameters);
                workerNodeStackRequest.setCapabilities(Collections.singletonList("CAPABILITY_IAM"));
                //This will never fail until you put in wrong parameters
                CreateStackResult resultStack = ((EksClient) baseClient).getAmazonCloudFormation().createStack(workerNodeStackRequest);
            }

        }

    }

    @Override
    public Status getClusterStatus(BaseClient baseClient) {
        EksConfig eksConfig = ((EksClient)baseClient).getConfig();
        DescribeClusterRequest describeClusterRequest = new DescribeClusterRequest().withName(eksConfig.getClusterName());
        try {
            DescribeClusterResult describeClusterResult = ((EksClient) baseClient).getEks().describeCluster(describeClusterRequest);
            String clusterStatus = describeClusterResult.getCluster().getStatus();
            if (clusterStatus.equalsIgnoreCase("creating"))
                return Status.PROVISIONING;
            else if (clusterStatus.equalsIgnoreCase("active"))
                return Status.RUNNING;
            else if (clusterStatus.equalsIgnoreCase("deleting"))
                return Status.STOPPING;
            else //  "Failed" message in EKS implies: no cluster is created
                return Status.NO_STATUS;
        }
        catch(com.amazonaws.services.eks.model.ResourceNotFoundException e){
            logger.warn(e.getMessage());
            return Status.NO_STATUS;
        }

    }

    @Override
    public Status getNodePoolStatus(BaseClient baseClient) {
        EksConfig eksConfig = ((EksClient)baseClient).getConfig();
        DescribeStacksRequest describeStacksRequest = new DescribeStacksRequest().withStackName(eksConfig.getPoolId());
        try {
            DescribeStacksResult describeStacksResult = ((EksClient) baseClient).getAmazonCloudFormation().describeStacks(describeStacksRequest);
            String nodePoolStatus = describeStacksResult.getStacks().get(0).getStackStatus();
            if (nodePoolStatus.equalsIgnoreCase("Create_in_progress"))
                return Status.PROVISIONING;
            else if (nodePoolStatus.equalsIgnoreCase("create_complete") || nodePoolStatus.equalsIgnoreCase("update_complete") )
                return Status.RUNNING;
            else if (nodePoolStatus.equalsIgnoreCase("delete_in_progress"))
                return Status.STOPPING;
            else if( nodePoolStatus.equalsIgnoreCase("delete_failed"))
                return Status.ERROR;
            else if(nodePoolStatus.equalsIgnoreCase("create_failed"))
                return Status.NO_STATUS;
            else if(nodePoolStatus.equalsIgnoreCase("update_in_progress"))
                return Status.UPDATING;
            else
                return Status.OTHERS;
        }
        catch(AmazonCloudFormationException e){
            logger.warn(e.getMessage());
            return Status.NO_STATUS;
        }

    }


    @Override
    public boolean createCluster(BaseClient baseClient) throws CloneNotSupportedException, InterruptedException {
        EksConfig eksConfig = ((EksClient)baseClient).getConfig();
        String keyName = getSshKeyPair(baseClient,eksConfig);
        Pair<String,List<String>> vpcSubnets = getVpcSubnet(baseClient, eksConfig);
        Role role = null;
        List<String> securityGroups = null;
        //This will be very rare event.
        if(vpcSubnets.getKey() == null) {
            logger.error("Failed to create Vpc/Subnet. Please Try again");
            return false;
        }
        else {
            role= getRole(baseClient, eksConfig);
            if(role== null){
                logger.error("Failed to create EKS Role. Please Try again");
                return false;
            }
            else{
                DescribeSecurityGroupsResult describeSecurityGroupsResult = ((EksClient) baseClient).getEc2().describeSecurityGroups();
                if(describeSecurityGroupsResult.getSecurityGroups()!= null){
                    securityGroups = describeSecurityGroupsResult.getSecurityGroups().stream()
                            .filter(securityGroup -> securityGroup.getVpcId().equalsIgnoreCase(vpcSubnets.getKey()))
                            .map(SecurityGroup::getGroupId)
                            .collect(Collectors.toList());
                }
                else{
                    logger.error("Failed to get Security group. Please Try again");
                    return false;
                }
            }

            final VpcConfigRequest vpcRequest = new VpcConfigRequest().withSecurityGroupIds(securityGroups).withSubnetIds(vpcSubnets.getValue());
            CreateClusterRequest request = new CreateClusterRequest().withName(eksConfig.getClusterName()).withVersion(eksConfig.getClusterVersion())
                    .withRoleArn(role.getArn()).withResourcesVpcConfig(vpcRequest);

            CreateClusterResult createClusterResult = ((EksClient) baseClient).getEks().createCluster(request);
            createNodePoolsOnEc2(baseClient, eksConfig, securityGroups, keyName, vpcSubnets);


        }
        return true;
    }

    @Override
    public boolean autoScaleCluster(BaseClient baseClient) {
        EksConfig eksConfig = ((EksClient)baseClient).getConfig();
        String key1 = "NodeAutoScalingGroupMinSize";
        String value1 = String.valueOf(eksConfig.getMinNodeCount());
        String key2 = "NodeAutoScalingGroupMaxSize";
        String value2 = String.valueOf(eksConfig.getMaxNodeCount());
        Collection<Parameter> nodeGroupParameters = new ArrayList<Parameter>();
        nodeGroupParameters.add(new Parameter().withParameterKey(key1).withParameterValue(value1));
        nodeGroupParameters.add(new Parameter().withParameterKey(key2).withParameterValue(value2));

        UpdateStackRequest updateStackRequest = new UpdateStackRequest().withStackName(eksConfig.getPoolId()).withParameters(nodeGroupParameters);
        UpdateStackResult  updateStackResult = ((EksClient) baseClient).getAmazonCloudFormation().updateStack(updateStackRequest);
        return true;
    }

    @Override
    public boolean scaleCluster(BaseClient client) {
        return false;
    }

    @Override
    public boolean updateCluster(BaseClient baseClient) {
        //Not Supported
        return false;
    }

    @Override
    public boolean deleteCluster(BaseClient baseClient)  {
        EksConfig eksConfig = ((EksClient)baseClient).getConfig();

        //delete Worker Nodes; VPC cant be deleted until all EC2 Nodepool are deleted.
        IntStream.range(0, eksConfig.getNoPools()).parallel().forEach(index ->
                ((EksClient) baseClient).getAmazonCloudFormation().deleteStack( new DeleteStackRequest().withStackName(eksConfig.getPoolIds().get(index))));
        int maxAttempts = 15;
        for(int index =0;index< eksConfig.getNoPools() ;) {
            DescribeStacksRequest describeStacksRequest = new DescribeStacksRequest().withStackName(eksConfig.getPoolIds().get(index));
            try{
                DescribeStacksResult describeStacksResult = ((EksClient) baseClient).getAmazonCloudFormation().describeStacks(describeStacksRequest);
		        maxAttempts--;
		        if(maxAttempts == 0)
		            break;
                Thread.sleep(30000);
            }
            catch(AmazonCloudFormationException e){
                logger.info(eksConfig.getPoolIds().get(index) +": is Deleted");
                index++;

            }
            catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
        }
        DescribeClusterRequest describeClusterRequest = new DescribeClusterRequest().withName(eksConfig.getClusterName());
        DescribeClusterResult describeClusterResult = ((EksClient) baseClient).getEks().describeCluster(describeClusterRequest);
        List<String> subnets = describeClusterResult.getCluster().getResourcesVpcConfig().getSubnetIds();
        String vpcId = describeClusterResult.getCluster().getResourcesVpcConfig().getVpcId();
        //delete Cluster, Subnet, Vpc
        DeleteClusterRequest deleteClusterRequest = new DeleteClusterRequest().withName(eksConfig.getClusterName());
        ((EksClient) baseClient).getEks().deleteCluster(deleteClusterRequest);
        if(eksConfig.getVpcType() == EksConfig.VpcType.NEW){
            maxAttempts = 15;
            while(true) {
               if(getClusterStatus(baseClient)!= Status.NO_STATUS){
                   try {
                       Thread.sleep(50000);
                   } catch (InterruptedException e) {
                       logger.error(e.getMessage());
                   }
                   maxAttempts--;
                   if(maxAttempts ==0)
                       break;
               }
               else{
                   break;
               }
            }
            subnets.parallelStream()
                    .forEach(subnet -> ((EksClient) baseClient).getEc2().deleteSubnet(new DeleteSubnetRequest().withSubnetId(subnet)));
            ((EksClient) baseClient).getEc2().deleteVpc(new DeleteVpcRequest().withVpcId(vpcId));
        }
        return true;
    }
    @Override
    public List<String> listClusters(BaseClient baseClient){
        ListClustersRequest listClustersRequest = new ListClustersRequest();
        List<String> clusters = ((EksClient) baseClient).getEks().listClusters(listClustersRequest).getClusters();
        if(clusters!= null)
            return clusters;
        else
            return new ArrayList<>();
    }

   public static EksProviderService getInstance(){

        return instance;
    }
    private EksProviderService() {
   
    }


}
