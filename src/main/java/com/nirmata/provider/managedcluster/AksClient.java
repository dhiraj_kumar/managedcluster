package com.nirmata.provider.managedcluster;

import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.credentials.ApplicationTokenCredentials;
import com.microsoft.azure.management.Azure;
import com.nirmata.provider.managedcluster.util.AksConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.UnknownHostException;


public final class AksClient extends BaseClient {
    private static final Logger logger = LoggerFactory.getLogger(AksClient.class);
    private static final Proxy proxy = initProxy();

    private static Proxy initProxy(){
        String host = System.getProperty("http.proxyHost");
        Proxy proxy = null;
        if(host!= null) {
            int port = Integer.valueOf(System.getProperty("http.proxyPort"));
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
        }
        return proxy;
    }

    private final AksConfig aksConfig;
    private final Azure azure;

    public Azure getAzure() {
        return azure;
    }

    public AksClient(AksConfig aksConfig) throws UnknownHostException,CloneNotSupportedException {
        this.aksConfig = aksConfig;
        ApplicationTokenCredentials credentials = new ApplicationTokenCredentials(
                aksConfig.getCredential().getClientID(), aksConfig.getCredential().getTenantId(), aksConfig.getCredential().getClientSecret(), AzureEnvironment.AZURE);
        Azure.Configurable configurable = Azure.configure();
        if(proxy!= null) {
            configurable = configurable.withProxy(proxy);
        }
        azure = configurable.authenticate(credentials).withSubscription(aksConfig.getCredential().getSubscriptionId());
    }

    public AksConfig getConfig() {
        return aksConfig;
    }
}

