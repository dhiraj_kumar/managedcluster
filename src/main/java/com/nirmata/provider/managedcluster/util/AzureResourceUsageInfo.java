package com.nirmata.provider.managedcluster.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AzureResourceUsageInfo {
    private static final Logger logger = LoggerFactory.getLogger(AzureResourceUsageInfo.class);
    private static final long serialVersionUID = -960355912464502932L;
    private AzureOauth2Token azureOauth2Token;
    private final AksConfig.Credential azureCredential;
    private final ObjectMapper objectMapper;

    private AzureUsage[] setResourcesUsages(String region) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("https://management.azure.com/subscriptions/"+ azureCredential.getSubscriptionId()+ "/providers/Microsoft.Compute/locations/"+region+"/usages?api-version=2017-12-01");
        httpGet.setHeader("Authorization", "Bearer "+ azureOauth2Token.getAccess_token());
        HttpResponse response = httpclient.execute(httpGet);
        if (response.getStatusLine().getStatusCode() != 200) {
            logger.error("Error: {}", response);
            throw new IOException("Failed to get AzureUsage for Region: "+region + "With HTTP error code : " + response.getStatusLine().getStatusCode());
        }

        BufferedInputStream jsonStream = new BufferedInputStream(response.getEntity().getContent());
        jsonStream.skip(14);
        return objectMapper.readValue(jsonStream, AzureUsage[].class);

    }

    private void setAzureOauth2Token() throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost post = new HttpPost("https://login.microsoftonline.com/"+azureCredential.getTenantId()+"/oauth2/token");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("grant_type","client_credentials"));
        params.add(new BasicNameValuePair("client_id", azureCredential.getClientID()));
        params.add(new BasicNameValuePair("client_secret", azureCredential.getClientSecret()));
        params.add(new BasicNameValuePair("resource","https://management.azure.com"));
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = httpclient.execute(post);
        if (response.getStatusLine().getStatusCode() != 200) {
            logger.error("Error: {}", response);
            throw new IOException("Failed to get Oauth2Token : HTTP error code : " + response.getStatusLine().getStatusCode());
        }
        azureOauth2Token = objectMapper.readValue(response.getEntity().getContent(), AzureOauth2Token.class);

    }
    public AzureResourceUsageInfo(AksConfig.Credential credential) throws IOException, ExecutionException {
        azureCredential = credential;
        SimpleModule module = new SimpleModule();
        module.addDeserializer(AzureOauth2Token.class, new AzureOauth2TokenDeserializer());
        module.addDeserializer(AzureUsage.class, new AzureUsageDeserializer());
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(module);
        setAzureOauth2Token();

    }


    private  String[] convertVMNameIntoRegex(String vmName){
        String[] regexArr = new String[2];
        regexArr[0] = StringUtils.join(vmName.replaceFirst("\\d+[ilmrt]?(-)?\\d*[ilmrt]?(_)*", "").split("_"), "").concat("Family");;
        regexArr[1] = StringUtils.join(vmName.split("_"), "");
        return regexArr;
    }

    private boolean checkNumberInRange( String source, String searchString){
        if(source.length()<5 || source.length() >7)
            return false;
        int dashIndex = source.indexOf("_");
        if(source.charAt(0) != source.charAt(dashIndex+1))
            return false;
        if(source.charAt(0) != searchString.charAt(0))
            return false;
        else{
            try {
                int minLimit = Integer.parseInt(source.substring(1,dashIndex));
                int maxLimit = Integer.parseInt(source.substring(dashIndex+2));
                int number = Integer.parseInt(searchString.substring(1));
                if(number >=minLimit && number <=maxLimit)
                    return true;
            }
            catch(Exception e){
                return false;
            }

        }
        return false;
    }

    private int getMaxNodeCount(AzureUsage [] azureUsageArr, String vmName){
        String[] localPattern = convertVMNameIntoRegex(vmName);
        Optional<AzureUsage> matchingVMCpuUsage = Arrays.stream(azureUsageArr).parallel().filter(azureUsage ->  azureUsage.getUsageName().getValue().matches("(?i:" +localPattern[0]+ ")")).findAny();
        AzureUsage finalMatchCpuUsage = null;
        if(!matchingVMCpuUsage.isPresent()) {

             /*
            Arrays.stream(azureUsageArr).parallel().filter(azureUsage -> {
                Matcher matcher1 = Pattern.compile("(.*)([A-Z]\\d+_[A-Z]\\d+)(Family)").matcher(azureUsage.getUsageName().getValue());
                if (!matcher1.find()) {
                   return false;
                }
                Matcher matcher2 = Pattern.compile("(.*)([A-Z]\\d+)(.*)").matcher(localPattern[1]);
                if (!matcher2.find()) {
                    return false;
                }
                if (checkNumberInRange(matcher1.group(2), matcher2.group(2))) {
                    return true;
                }
                return false;
            }).findAny();
            */
            for (AzureUsage azureUsage : azureUsageArr) {
                Matcher matcher1 = Pattern.compile("(.*)([A-Z]\\d+_[A-Z]\\d+)(Family)").matcher(azureUsage.getUsageName().getValue());
                if (!matcher1.find()) {
                    continue;
                }
                Matcher matcher2 = Pattern.compile("(.*)([A-Z]\\d+)(.*)").matcher(localPattern[1]);
                if (!matcher2.find()) {
                    continue;
                }

                if (checkNumberInRange(matcher1.group(2), matcher2.group(2))) {
                    finalMatchCpuUsage = azureUsage;
                    break;
                }
            }
        }

        else{
            finalMatchCpuUsage = matchingVMCpuUsage.get();
        }
        Optional<AzureUsage>regionalCpuUsage = Arrays.stream(azureUsageArr).parallel().filter(azureUsage ->  azureUsage.getUsageName().getValue().contains("cores")).findAny();

        if(regionalCpuUsage.isPresent()){
            AzureUsage totalRegionalCpuUsage = regionalCpuUsage.get();
            if(finalMatchCpuUsage!= null){
                return Math.min(finalMatchCpuUsage.getLimit() - finalMatchCpuUsage.getCurrentValue(), totalRegionalCpuUsage.getLimit()- totalRegionalCpuUsage.getCurrentValue());
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    public int getMaxNodeCount(String region, String vmName) throws IOException {
        return getMaxNodeCount(setResourcesUsages(region), vmName);
    }
}
