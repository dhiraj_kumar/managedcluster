package com.nirmata.provider.managedcluster.util;

public final class AksConfig extends  Config {
    public enum ResourceType {
        EXISTING, NEW;
    }
    public enum OsType{
        LINUX;
    }
    private final Credential credential;
    private final String resourceGroup;
    private final String region;
    private ResourceType resourceType;
    private String clusterVersion;
    private String machineType;
    private int nodeCount;
    private int diskSize;
    private OsType osType;

    private AksConfig(Builder builder){
        super(ProviderType.AKS, builder.clusterName);
        credential = builder.credential;
        resourceType = builder.resourceType;
        resourceGroup = builder.resourceGroup;
        region = builder.region;
        clusterVersion = builder.clusterVersion;
        machineType = builder.machineType;
        nodeCount = builder.nodeCount;
        diskSize = builder.diskSize;
        osType = builder.osType;
    }
    public static class Credential implements  Cloneable {
        private final String clientID;
        private final String clientSecret;
        private final String tenantId;
        private final String subscriptionId;
        public Credential(String clientId, String clientSecret, String tenantId, String subscriptionId) {
            this.clientID = clientId;
            this.clientSecret = clientSecret;
            this.tenantId = tenantId;
            this.subscriptionId = subscriptionId;
        }
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public String getClientID() {
            return clientID;
        }

        public String getSubscriptionId() {
            return subscriptionId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public String getTenantId() {
            return tenantId;
        }
    }

    public String getRegion() {
        return region;
    }

    public String getMachineType() {
        return machineType;
    }

    public String getClusterVersion() {
        return clusterVersion;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public Credential getCredential() throws CloneNotSupportedException {
        return (Credential) credential.clone();
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public String getResourceGroup() {
        return resourceGroup;
    }

    public int getDiskSize() {
        return diskSize;
    }

    public OsType getOsType() {
        return osType;
    }

    public static class Builder {

        private final Credential credential;
        private final String clusterName;
        private final String resourceGroup;
        private final String region;
        private ResourceType resourceType = ResourceType.NEW;
        private String clusterVersion;
        private String machineType = "Standard_DS2_v2";
        private int nodeCount = 1;
        private OsType osType = OsType.LINUX;
        private int diskSize;

        public Builder(Credential credential, String clusterName, String resourceGroup, String region) throws CloneNotSupportedException {
            this.credential = (Credential)credential.clone();
            this.clusterName = clusterName;
            this.resourceGroup = resourceGroup;
            this.region = region;
        }

        public Builder setResourceType(ResourceType resourceType) {
            this.resourceType = resourceType;
            return this;
        }

        public Builder setClusterVersion(String clusterVersion) {
            this.clusterVersion = clusterVersion;
            return this;
        }

        public Builder setMachineType(String machineType) {
            this.machineType = machineType;
            return this;
        }

        public Builder setNodeCount(int nodeCount) {
            this.nodeCount = nodeCount;
            return this;
        }

        public Builder setOsType(OsType osType) {
            this.osType = osType;
            return this;
        }

        public Builder setDiskSize(int diskSize) {
            this.diskSize = diskSize;
            return this;
        }

        public AksConfig build(){
            return new AksConfig(this);
        }
    }

}
