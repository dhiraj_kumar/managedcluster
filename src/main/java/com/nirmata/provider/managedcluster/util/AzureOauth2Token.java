package com.nirmata.provider.managedcluster.util;

public class AzureOauth2Token {
    private String token_type;
    private int expires_in;
    private int ext_expires_in;
    private int expires_on;
    private int not_before;
    private String resource;
    private String access_token;

    public AzureOauth2Token(String token_type, int expires_in, int ext_expires_in, int expires_on , int not_before, String resource, String access_token){
        this.token_type = token_type;
        this.expires_in = expires_in;
        this.ext_expires_in  = ext_expires_in;
        this.expires_on = expires_on;
        this.not_before = not_before;
        this.resource = resource;
        this.access_token = access_token;
    }

    public int getExpires_on() {
        return expires_on;
    }

    public String getAccess_token() {
        return access_token;
    }
}

