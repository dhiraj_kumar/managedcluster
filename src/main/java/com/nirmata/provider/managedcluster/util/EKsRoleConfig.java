package com.nirmata.provider.managedcluster.util;

public final class EKsRoleConfig {
    private static final String sshKeyName  = "eks_key";
    private static final String roleName  = "eks_aws";
    private static final String nodePoolTemplate = "https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/amazon-eks-nodegroup.yaml";
    private static final String roleDescription = "Allows EKS to manage clusters on your behalf.";
    private static final String eksClusterPolicyArn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy";
    private static final String eksServicePolicyArn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy";
    private static final String rolePolicyDoc = "{\"Version\": \"2012-10-17\","
            + "\"Statement\": ["
            + "{"
            + "\"Effect\": \"Allow\","
            + "\"Principal\": {"
            + "\"Service\": \"eks.amazonaws.com\""
            + "},"
            + "\"Action\": \"sts:AssumeRole\""
            + "}"
            + "]"
            + "}";
    private static final String eksClusterPolicyDoc = "{\n" +
            "    \"Version\": \"2012-10-17\",\n" +
            "    \"Statement\": [\n" +
            "        {\n" +
            "            \"Effect\": \"Allow\",\n" +
            "            \"Action\": [\n" +
            "                \"ec2:CreateNetworkInterface\",\n" +
            "                \"ec2:CreateNetworkInterfacePermission\",\n" +
            "                \"ec2:DeleteNetworkInterface\",\n" +
            "                \"ec2:DescribeInstances\",\n" +
            "                \"ec2:DescribeNetworkInterfaces\",\n" +
            "                \"ec2:DescribeSecurityGroups\",\n" +
            "                \"ec2:DescribeSubnets\",\n" +
            "                \"ec2:DescribeVpcs\",\n" +
            "                \"ec2:ModifyNetworkInterfaceAttribute\",\n" +
            "                \"iam:ListAttachedRolePolicies\"\n" +
            "            ],\n" +
            "            \"Resource\": \"*\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"Effect\": \"Allow\",\n" +
            "            \"Action\": [\n" +
            "                \"ec2:CreateTags\",\n" +
            "                \"ec2:DeleteTags\"\n" +
            "            ],\n" +
            "            \"Resource\": [\n" +
            "                \"arn:aws:ec2:*:*:vpc/*\",\n" +
            "                \"arn:aws:ec2:*:*:subnet/*\"\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    private static final String eksServicePolicyDoc = "{\n" +
            "    \"Version\": \"2012-10-17\",\n" +
            "    \"Statement\": [\n" +
            "        {\n" +
            "            \"Effect\": \"Allow\",\n" +
            "            \"Action\": [\n" +
            "                \"ec2:CreateNetworkInterface\",\n" +
            "                \"ec2:CreateNetworkInterfacePermission\",\n" +
            "                \"ec2:DeleteNetworkInterface\",\n" +
            "                \"ec2:DescribeInstances\",\n" +
            "                \"ec2:DescribeNetworkInterfaces\",\n" +
            "                \"ec2:DescribeSecurityGroups\",\n" +
            "                \"ec2:DescribeSubnets\",\n" +
            "                \"ec2:DescribeVpcs\",\n" +
            "                \"ec2:ModifyNetworkInterfaceAttribute\",\n" +
            "                \"iam:ListAttachedRolePolicies\"\n" +
            "            ],\n" +
            "            \"Resource\": \"*\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"Effect\": \"Allow\",\n" +
            "            \"Action\": [\n" +
            "                \"ec2:CreateTags\",\n" +
            "                \"ec2:DeleteTags\"\n" +
            "            ],\n" +
            "            \"Resource\": [\n" +
            "                \"arn:aws:ec2:*:*:vpc/*\",\n" +
            "                \"arn:aws:ec2:*:*:subnet/*\"\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public static String getEksClusterPolicyDoc() {
        return eksClusterPolicyDoc;
    }

    public static String getEksServicePolicyDoc() {
        return eksServicePolicyDoc;
    }

    public static String getRolePolicyDoc() {
        return rolePolicyDoc;
    }

    public static String getRoleName() {
        return roleName;
    }

    public static String getRoleDescription() {
        return roleDescription;
    }

    public static String getSshKeyName() {
        return sshKeyName;
    }

    public static String getEksClusterPolicyArn() {
        return eksClusterPolicyArn;
    }

    public static String getEksServicePolicyArn() {
        return eksServicePolicyArn;
    }

    public static String getNodePoolTemplate() {
        return nodePoolTemplate;
    }

    private EKsRoleConfig (){

    }
}
