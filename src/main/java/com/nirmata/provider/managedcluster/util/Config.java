package com.nirmata.provider.managedcluster.util;

import java.io.Serializable;

public class Config implements Serializable,Cloneable {
    public enum ProviderType{
        GKE,EKS,AKS;
    }
    private final ProviderType providerType;
    private final String clusterName;

    public Config.ProviderType getProviderType() {
        return providerType;
    }

    public String getClusterName() {
        return clusterName;
    }

    public Config(ProviderType providerType, String clusterName){
        this.providerType = providerType;
        this.clusterName = clusterName;
    }

}

