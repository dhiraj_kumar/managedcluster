package com.nirmata.provider.managedcluster.util;

import com.google.common.base.MoreObjects;

public class AzureUsage {
    private int currentValue;
    private int limit;
    public int getCurrentValue() {
        return currentValue;
    }
    public int getLimit() {
        return limit;
    }
    public String getUsageUnit() {
        return usageUnit;
    }
    public UsageName getUsageName() {
        return usageName;
    }
    public class UsageName {
        String localizedValue;
        String value;
        public UsageName(String value,String localizedValue){
            this.value = value;
            this.localizedValue = localizedValue;
        }
        public String getLocalizedValue() {
            return localizedValue;
        }
        public String getValue() {
            return value;
        }
    }

    private UsageName usageName;
    private String usageUnit;

    public AzureUsage(int currentValue, int limit, String value, String localizedValue, String usageUnit) {
        this.currentValue = currentValue;
        this.limit = limit;
        this.usageName = new UsageName(value, localizedValue);
        this.usageUnit = usageUnit;

    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("AzureUsage")
                .add("currentValue", currentValue)
                .add("limit", limit)
                .add("localizedValue", this.usageName.localizedValue)
                .add("value", this.usageName.value)
                .add("usageUnit", usageUnit)
                .toString();
    }
}