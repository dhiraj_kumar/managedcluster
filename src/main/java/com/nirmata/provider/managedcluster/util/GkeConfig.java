package com.nirmata.provider.managedcluster.util;
import com.google.api.services.container.model.NodeConfig;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class GkeConfig extends  Config implements  Cloneable, Serializable {
     private final String credentialFile;
     private final String projectId;
     private final String zone;
     private String applicationName;
     private final int initialNodeCount;
     private final int[] initialNodeCounts;
     private String clusterVersion;
     private String nodeVersion;
     private int noPools;
     public enum PoolType {
        DEFAULT, OTHERS;
     }
     private PoolType poolType;
     private String poolId;
     private List<String> poolIds;
     private boolean isAutoScalingEnabled;
     private boolean [] isAutoScalingEnabledArr;
     private int minNodeCount;
     private int[] minNodeCounts = null;
     private int maxNodeCount;
     private int[] maxNodeCounts = null;
     private NodeConfig nodeConfig;
     private NodeConfig [] nodeConfigs;
     private String machineType;
     private int diskSize;
     private String imageType;
     private boolean isPrivateClusterEnabled;
     private boolean isVpcEnabled;
     public Object clone() throws CloneNotSupportedException {
        return super.clone();
     }

     public String getClusterVersion() {
        return clusterVersion;
     }

     public String getNodeVersion() {
        return nodeVersion;
     }

     public String getApplicationName() {
        return applicationName;
     }

     public PoolType getPoolType() {
        return poolType;
     }

     public String getPoolId() {
        return poolId;
     }

     public boolean isAutoScalingEnabled() {
        return isAutoScalingEnabled;
     }

     public int getMinNodeCount() {
        return minNodeCount;
     }

     public int getMaxNodeCount() {
        return maxNodeCount;
     }

     public int getNoPools() {
        return noPools;
     }

     public List<String> getPoolIds() {
        return poolIds.stream().collect(Collectors.toList());
     }

     public int getInitialNodeCount() {
        return initialNodeCount;
     }

     public String getCredentialFile() {
        return credentialFile;
     }

     public String getImageType() {
        return imageType;
     }

     public String getProjectId() {
        return projectId;
     }

     public String getZone() {
        return zone;
     }

     public NodeConfig getNodeConfig() {
        return nodeConfig;
     }

     public NodeConfig[] getNodeConfigs() {
        return nodeConfigs.clone();
     }

     public boolean  getIsPrivateClusterEnabled() {
        return isPrivateClusterEnabled;
     }

     public int getDiskSize() {
        return diskSize;
     }

     public String getMachineType() {
        return machineType;
     }

     public boolean[] getIsAutoScalingEnabledArr() {
        return isAutoScalingEnabledArr.clone();
     }

     public int[] getMinNodeCounts() {
        return minNodeCounts.clone();
     }

     public int[] getMaxNodeCounts() {
        return maxNodeCounts.clone();

     }

     public int[] getInitialNodeCounts() {
        return initialNodeCounts.clone();

     }

     public boolean getIsVpcEnabled(){
        return isVpcEnabled;
     }

     private GkeConfig(Builder builder) {
        super(ProviderType.GKE, builder.clusterName);
        credentialFile = builder.credentialFile;
        projectId = builder.projectId;
        initialNodeCount = builder.initialNodeCount;
        zone = builder.zone;
        applicationName = builder.applicationName;
        poolType =builder.poolType;
        isAutoScalingEnabled = builder.isAutoScalingEnabled;
        imageType = builder.imageType;
        diskSize = builder.diskSize;
        machineType = builder.machineType;
        clusterVersion = builder.clusterVersion;
        nodeVersion = builder.nodeVersion;
        nodeConfig = new NodeConfig();
        if(imageType != null)
            nodeConfig.setImageType(imageType);
        if(diskSize >= 10)
            nodeConfig.setDiskSizeGb(diskSize);
        if(machineType!= null)
            nodeConfig.setMachineType(machineType);
        if(builder.isAutoScalingEnabled){
            isAutoScalingEnabled = true;
            minNodeCount = builder.minNodeCount;
            maxNodeCount = builder.maxNodeCount;
        }
        noPools = builder.noPools;
        initialNodeCounts = builder.initialNodeCounts;
        if(noPools == 1)
            poolId = builder.poolId;
        else {
            isAutoScalingEnabledArr = builder.isAutoScalingEnabledArr;
            poolIds = Arrays.asList(builder.poolIds);
            minNodeCounts = new int[noPools];
            maxNodeCounts = new int[noPools];
            minNodeCounts = builder.minNodeCounts;
            maxNodeCounts = builder.maxNodeCounts;
            nodeConfigs = new NodeConfig[noPools];
            for(int index = 0; index <noPools; index++){
                nodeConfigs[index] = new NodeConfig();
                if(builder.imageTypes[index]!= null) {
                    nodeConfigs[index].setImageType(builder.imageTypes[index]);
                }
                else {
                    if(imageType!= null)
                        nodeConfigs[index].setImageType(imageType);
                }
                if(builder.diskSizes[index] >= 10)
                    nodeConfigs[index].setDiskSizeGb(builder.diskSizes[index]);
                else{
                    if(diskSize>=10)
                        nodeConfigs[index].setDiskSizeGb(diskSize);
                }
                if(builder.machineTypes[index]!= null)
                    nodeConfigs[index].setMachineType(builder.machineTypes[index]);
                else{
                    if(machineType!= null)
                        nodeConfigs[index].setMachineType(machineType);
                }

            }
        }
        if(builder.isPrivateClusterEnabled) {
            isVpcEnabled = true;
        }

     }
     public static class Builder{
        public final String clusterName;
        private final String credentialFile;
        private final String projectId;
        private String applicationName = "Sample";
        private int initialNodeCount = 1;
        private int[] initialNodeCounts;
        private String clusterVersion;
        private String nodeVersion;
        private boolean isVpcEnabled;
        private boolean isPrivateClusterEnabled;
        private int noPools = 1;
        private String [] poolIds;
        private final String zone;
        private String machineType;
        private String[] machineTypes;
        private int diskSize;
        private int[] diskSizes;
        private String imageType;
        private String[] imageTypes;
        private GkeConfig.PoolType poolType = PoolType.DEFAULT;
        private String poolId = "default-pool";
        private boolean isAutoScalingEnabled;
        private boolean [] isAutoScalingEnabledArr;
        private int minNodeCount = 1;
        private int[] minNodeCounts;
        private int maxNodeCount = 3;
        private int[] maxNodeCounts;
        public Builder(String credentialFile, String clusterName, String projectId,  String zone){
            this.credentialFile = credentialFile;
            this.clusterName = clusterName;
            this.projectId = projectId;
            this.zone = zone;
        }
        private void initValues(){
            if(diskSizes == null){
                diskSizes = new int[noPools];
            }
            if(imageTypes == null){
                imageTypes = new String[noPools];
            }
            if(machineTypes == null){
                machineTypes = new String[noPools];
            }
            if(initialNodeCounts == null){
                initialNodeCounts = new int[noPools];
                Arrays.fill(initialNodeCounts,initialNodeCount);
            }
            if(isAutoScalingEnabledArr == null){
                isAutoScalingEnabledArr = new boolean[noPools];
                if(minNodeCounts == null){
                    minNodeCounts = new int[noPools];
                    Arrays.fill(minNodeCounts,minNodeCount);
                }
                if(maxNodeCounts == null){
                    maxNodeCounts = new int[noPools];
                    Arrays.fill(maxNodeCounts,maxNodeCount);
                }
            }
        }

        public Builder setApplicationName(String applicationName) {
            this.applicationName = applicationName;
            return this;
        }

        public Builder setNoPools(int noPools){
            this.noPools = noPools;
            initValues();
            return this;
        }
        public Builder setPoolIds(String [] poolIds){
            this.poolType = PoolType.OTHERS;
            this.noPools = poolIds.length;
            if(poolIds.length>0)
                this.poolIds = poolIds.clone();
            initValues();
            return this;
        }

        public Builder setPrivateClusterEnabled(boolean privateClusterEnabled) {
            isPrivateClusterEnabled = privateClusterEnabled;
            if(privateClusterEnabled) {
                isVpcEnabled = true;
            }
            return this;
        }

        public Builder setVpcEnabled(boolean vpcEnabled) {
            isVpcEnabled = vpcEnabled;
            return this;
        }

        public Builder setInitialNodeCount(int initialNodeCount) {
            this.initialNodeCount = initialNodeCount;
            return this;
        }

        public Builder setInitialNodeCount(int[] initialNodeCounts){
            initValues();
            this.initialNodeCounts= initialNodeCounts.clone();
            return this;
        }
        public Builder setClusterVersion(String clusterVersion) {
            this.clusterVersion = clusterVersion;
            return this;
        }

        public Builder setNodeVersion(String nodeVersion) {
            this.nodeVersion = nodeVersion;
            return this;
        }

        public Builder setAutoScaling(boolean isAutoScalingEnabled) {
            this.isAutoScalingEnabled = isAutoScalingEnabled;
            return this;
        }

        public Builder setAutoScaling(boolean [] isAutoScalingEnabledArr){
            initValues();
            this.isAutoScalingEnabledArr = isAutoScalingEnabledArr.clone();
            return this;
        }
        public Builder setMinNodeCount(int minNodeCount) {
            this.minNodeCount = minNodeCount;
            return this;
        }

        public Builder setMinNodeCount(int[] minNodeCounts) {
            initValues();
            this.minNodeCounts = minNodeCounts.clone();
            return this;
        }
        public Builder setMaxNodeCount(int maxNodeCount) {
            if(maxNodeCount > 0)
                this.maxNodeCount = maxNodeCount;
            return this;
        }


        public Builder setMaxNodeCount(int[] maxNodeCounts) {
            initValues();
            this.maxNodeCounts = maxNodeCounts.clone();
            return this;
        }
        public Builder setPoolType(PoolType poolType) {
            this.poolType = poolType;
            return this;
        }

        public Builder setPoolId(String poolId) {
            this.poolId = poolId;
            this.poolType = PoolType.OTHERS;
            return this;
        }

        public Builder setMachineType(String machineType) {
            this.machineType = machineType;
            return this;
        }


        public Builder setMachineType(String[] machineTypes) {
            initValues();
            this.machineTypes = this.machineTypes.clone();
            return this;
        }

        public  Builder  setDiskSize(int diskSize) {
            this.diskSize = diskSize;
            return this;
        }

        public  Builder  setDiskSize(int [] diskSizes) {
            initValues();
            this.diskSizes = diskSizes;
            return this;
        }
        public Builder setImageType(String imageType) {
            this.imageType = imageType;
            return this;
        }

        public Builder setImageType(String[] imageTypes) {
            initValues();
            this.imageTypes = imageTypes.clone();
            return this;
        }
        public GkeConfig build() {
            return new GkeConfig(this);
        }
     }

}

