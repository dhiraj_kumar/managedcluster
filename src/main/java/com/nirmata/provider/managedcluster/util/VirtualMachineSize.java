package com.nirmata.provider.managedcluster.util;

import com.google.common.base.MoreObjects;

public class VirtualMachineSize {
    private int maxDataDiskCount;
    private int memoryInMB;
    private String name;
    private int numberOfCores;
    private int osDiskSizeInMB;
    private int resourceDiskSizeInMB;

    public VirtualMachineSize(String name, int numberOfCores,
                              int osDiskSizeInMB, int resourceDiskSizeInMB, int memoryInMB, int maxDataDiskCount ){
        this.maxDataDiskCount = maxDataDiskCount;
        this.memoryInMB = memoryInMB;
        this.name = name;
        this.numberOfCores = numberOfCores;
        this.osDiskSizeInMB = osDiskSizeInMB;
        this.resourceDiskSizeInMB = resourceDiskSizeInMB;
    }

    public int getMaxDataDiskCount() {
        return maxDataDiskCount;
    }

    public int getMemoryInMB() {
        return memoryInMB;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }

    public int getOsDiskSizeIMB() {
        return osDiskSizeInMB;
    }

    public int getResourceDiskSizeInMB() {
        return resourceDiskSizeInMB;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("VirtualMachineSize")
                .add("name", name)
                .add("numberOfCores", numberOfCores)
                .add("osDiskSizeInMB", osDiskSizeInMB)
                .add("resourceDiskSizeInMB", resourceDiskSizeInMB)
                .add("memoryInMB", memoryInMB)
                .add("maxDataDiskCount", maxDataDiskCount)
                .toString();
    }
}
