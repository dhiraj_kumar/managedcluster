package com.nirmata.provider.managedcluster.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.ImmutableList;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AksVMInfo {
    private static final Logger logger = LoggerFactory.getLogger(AksVMInfo.class);
    private static final long serialVersionUID = -960355912464502932L;
    private AzureOauth2Token azureOauth2Token;
    private final AksConfig.Credential azureCredential;
    private final ObjectMapper objectMapper;

    private VirtualMachineSize[] setVirtualMachineSizes(String region) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("https://management.azure.com/subscriptions/"+ azureCredential.getSubscriptionId()+ "/providers/Microsoft.Compute/locations/"+region+"/vmSizes?api-version=2017-12-01");
        httpGet.setHeader("Authorization", "Bearer "+ azureOauth2Token.getAccess_token());
        HttpResponse response = httpclient.execute(httpGet);
        if (response.getStatusLine().getStatusCode() != 200) {
            logger.error("Error: {}", response);
            throw new IOException("Failed to get VirtualMachineSizes for Region: "+region + "With HTTP error code : " + response.getStatusLine().getStatusCode());
        }
        BufferedInputStream jsonStream = new BufferedInputStream(response.getEntity().getContent());
        jsonStream.skip(14);
        return objectMapper.readValue(jsonStream, VirtualMachineSize[].class);

    }

    private void setAzureOauth2Token() throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost post = new HttpPost("https://login.microsoftonline.com/"+azureCredential.getTenantId()+"/oauth2/token");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("grant_type","client_credentials"));
        params.add(new BasicNameValuePair("client_id", azureCredential.getClientID()));
        params.add(new BasicNameValuePair("client_secret", azureCredential.getClientSecret()));
        params.add(new BasicNameValuePair("resource","https://management.azure.com"));
        post.setEntity(new UrlEncodedFormEntity(params));
        HttpResponse response = httpclient.execute(post);
        if (response.getStatusLine().getStatusCode() != 200) {
            logger.error("Error: {}", response);
            throw new IOException("Failed to get Oauth2Token : HTTP error code : " + response.getStatusLine().getStatusCode());
        }
        azureOauth2Token = objectMapper.readValue(response.getEntity().getContent(), AzureOauth2Token.class);

    }
    public AksVMInfo(AksConfig.Credential credential) throws IOException, ExecutionException {
        azureCredential = credential;
        SimpleModule module = new SimpleModule();
        module.addDeserializer(AzureOauth2Token.class, new AzureOauth2TokenDeserializer());
        module.addDeserializer(VirtualMachineSize.class, new VirtualMachineSizeDeserializer());
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);
        objectMapper.registerModule(module);
        setAzureOauth2Token();

    }

    public List<VirtualMachineSize> getVirtualMachineSizes(String Region) throws ExecutionException, IOException {
        return ImmutableList.copyOf(setVirtualMachineSizes(Region));
    }
}
