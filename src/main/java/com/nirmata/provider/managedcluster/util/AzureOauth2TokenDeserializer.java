package com.nirmata.provider.managedcluster.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class AzureOauth2TokenDeserializer  extends StdDeserializer<AzureOauth2Token> {

    private static final Logger logger = LoggerFactory.getLogger(AzureOauth2TokenDeserializer.class);
    private static final long serialVersionUID = -960355912464501932L;

    public AzureOauth2TokenDeserializer() {
        super(AzureOauth2Token.class);
    }

    @Override
    public AzureOauth2Token deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String token_type = node.get("token_type").asText();
        int expires_in = Integer.valueOf(node.get("expires_in").asText());
        int ext_expires_in = Integer.valueOf( node.get("ext_expires_in").asText());
        int expires_on = Integer.valueOf (node.get("expires_on").asText());
        int not_before = Integer.valueOf( node.get("not_before").asText());
        String resource = node.get("resource").asText();
        String access_token = node.get("access_token").asText();
        return new AzureOauth2Token(token_type, expires_in, ext_expires_in, expires_on, not_before, resource, access_token);
    }
}
