package com.nirmata.provider.managedcluster.util;

import java.util.Random;

public class IPv4CIDRBlock {
    private IPv4CIDRBlock (){

    }
    private static int clearBits(int ip, int bitsToClear){
        int mask = ~((1<<bitsToClear)-1);
        return ip & mask;

    }

    public static String getCIDRVpc(){
        Random random = new Random();
        int mask = 16;
        int prefixMask = 16; // 32 - mask;
        int randomPrivateIp = (10<<24)+ (random.nextInt(256) <<16);
        int maskedPrivateIp = clearBits(randomPrivateIp,mask);
        return new StringBuilder().append((maskedPrivateIp>>24)&0xFF).append(".").append((maskedPrivateIp>>16)&0xFF).append(".")
                .append(0).append(".").append(0).append("/").append(prefixMask).toString();
    }
    /*Get Random 20 Bit CIDR Subnet for given 16 bit Vpc. With 20 bit subnet, we can support 4096 hosts for a subnet.
    3 Subnet in medium sized organization can  serve up to 80 worker nodes. Each Pod needs an unique IP address.
    At its maximum capacity; 32 core, 32 GB RAM can handle 150 pods.( 150 pods * 80 nodes = 12000 < 4096 * 3)
     A pod can have many containers and they use port number relative to pod ip address
     */
    public static String getCIDRSubnet(String vpcCIDR, int randomCounter){
        String [] split = vpcCIDR.split("\\.");
        return split[0]+"." +split[1]+ "." +(randomCounter<<4) + ".0" +
                "/" +(20);
    }
}
