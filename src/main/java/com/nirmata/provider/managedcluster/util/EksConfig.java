package com.nirmata.provider.managedcluster.util;

import com.amazonaws.services.cloudformation.model.Parameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class EksConfig extends  Config {
    private EksConfig(Builder builder){
        super(ProviderType.EKS,builder.clusterName);
        vpcType = builder.vpcType;
        credential = builder.credential;
        region = builder.region;
        clusterVersion = builder.clusterVersion;
        machineType = builder.machineType;
        machineTypes = Arrays.asList(builder.machineTypes);
        initialNodeCount = builder.initialNodeCount;
        minNodeCount = builder.minNodeCount;
        maxNodeCount = builder.maxNodeCount;
        amiType = builder.amiType;
        amiTypes = Arrays.asList(builder.amiTypes);
        poolType =builder.poolType;
        noPools = builder.noPools;
        initialNodeCounts = builder.initialNodeCounts;
        if(noPools == 1)
            poolId = builder.poolId;
        else {
            poolIds = Arrays.asList(builder.poolIds);
            minNodeCounts = new int[noPools];
            maxNodeCounts = new int[noPools];
            minNodeCounts = builder.minNodeCounts;
            maxNodeCounts = builder.maxNodeCounts;
            for(int index = 0; index <noPools; index++){
                if(builder.amiTypes[index]!= null) {
                    amiTypes.set(index,builder.amiTypes[index]);
                }
                else {
                    if(builder.amiType!= null)
                        amiTypes.set(index,amiType);
                }
                if(builder.machineTypes[index]!= null){
                    machineTypes.set(index,builder.machineTypes[index]);
                }
                else{
                    if(builder.machineType!= null)
                        machineTypes.set(index, machineType);
                }

            }
        }
    }
    public enum VpcType {
        DEFAULT, NEW;
    }
    private VpcType vpcType;
    private EksConfig.Credential credential;
    private String clusterName;
    private String region;
    private String clusterVersion;
    private String machineType;
    private List<String> machineTypes;
    private String amiType;
    private int initialNodeCount;
    private int[] initialNodeCounts;
    private int minNodeCount;
    private int[] minNodeCounts;
    private int maxNodeCount;
    private int[] maxNodeCounts;
    private int noPools;
    public enum PoolType {
        DEFAULT, OTHERS;
    }
    private PoolType poolType;
    private String poolId;
    private List<String> poolIds;
    private List<String> amiTypes;

    public int getMinNodeCount() {
        return minNodeCount;
    }

    public int getMaxNodeCount() {
        return maxNodeCount;
    }

    public int getNoPools() {
        return noPools;
    }

    public List<String> getPoolIds() {
        return poolIds.stream().collect(Collectors.toList());
    }


    public PoolType getPoolType() {
        return poolType;
    }

    public String getPoolId() {
        return poolId;
    }
    public String getRegion() {
        return region;
    }

    public VpcType getVpcType() {
        return vpcType;
    }

    public Credential getCredential() throws CloneNotSupportedException {
        return (Credential) credential.clone();
    }

    public String getAmiType() {
        return amiType;
    }

    public int getInitialNodeCount() {
        return initialNodeCount;
    }
    public int[] getMinNodeCounts() {
        return minNodeCounts.clone();
    }

    public int[] getMaxNodeCounts() {
        return maxNodeCounts.clone();

    }

    public int[] getInitialNodeCounts() {
        return initialNodeCounts.clone();

    }

    public String getClusterVersion() {
        return clusterVersion;
    }

    public String getMachineType() {
        return machineType;
    }

    public List<String> getAmiTypes() {
        return amiTypes.stream().collect(Collectors.toList());
    }

    public List<String> getMachineTypes() {
        return machineTypes.stream().collect(Collectors.toList());
    }

    public static class Credential implements Cloneable{
        private final String accessKey;
        private final String secretKey;
        public Credential(String accessKey, String secretKey){
            this.accessKey = accessKey;
            this.secretKey = secretKey;
        }
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public String getAccessKey() {
            return accessKey;
        }

        public String getSecretKey() {
            return secretKey;
        }
    }
    public static class Builder{
        private final EksConfig.Credential credential;
        private final String clusterName;
        private final String region;
        private VpcType vpcType =VpcType.NEW;
        private String clusterVersion;
        private String machineType = "t2.small";
        private String [] machineTypes;
        private String amiType = "ami-a0cfeed8";
        private String [] amiTypes;
        private int noPools = 1;
        private String [] poolIds;
        private EksConfig.PoolType poolType = PoolType.DEFAULT;
        private String poolId = "default-pool";
        private int initialNodeCount = 1;
        private int[] initialNodeCounts;
        private int minNodeCount = 1;
        private int[] minNodeCounts;
        private int maxNodeCount = 2;
        private int[] maxNodeCounts;

        public Builder(Credential credential, String clusterName, String region) throws CloneNotSupportedException {
            this.credential = (Credential) credential.clone();
            this.clusterName = clusterName;
            this.region = region;
        }
        private void initValues(int noPools){
            if(amiTypes == null){
                amiTypes = new String[noPools];
            }
            if(machineTypes == null){
                machineTypes = new String[noPools];
            }
            if(initialNodeCounts == null){
                initialNodeCounts = new int[noPools];
                Arrays.fill(initialNodeCounts,initialNodeCount);
            }

            if(minNodeCounts == null){
                minNodeCounts = new int[noPools];
                Arrays.fill(minNodeCounts,minNodeCount);
            }
            if(maxNodeCounts == null) {
                maxNodeCounts = new int[noPools];
                Arrays.fill(maxNodeCounts, maxNodeCount);
            }
        }

        public Builder setNoPools(int noPools){
            this.noPools = noPools;
            initValues(noPools);
            return this;
        }
        public Builder setPoolIds(String [] poolIds){
            this.poolType = PoolType.OTHERS;
            this.noPools = poolIds.length;
            if(poolIds.length>0)
                this.poolIds = poolIds.clone();
            initValues(noPools);
            return this;
        }

        public Builder setInitialNodeCount(int[] initialNodeCounts){
            this.initialNodeCounts= initialNodeCounts.clone();
            return this;
        }
        public Builder setMinNodeCount(int minNodeCount) {
            if(minNodeCount > 0)
                this.minNodeCount = minNodeCount;
            return this;
        }

        public Builder setMinNodeCount(int[] minNodeCounts) {
            initValues(minNodeCounts.length);
            this.minNodeCounts = minNodeCounts.clone();
            return this;
        }

        public Builder setMaxNodeCount(int maxNodeCount) {
            if(maxNodeCount > 0)
                this.maxNodeCount = maxNodeCount;
            return this;
        }

        public Builder setMaxNodeCount(int[] maxNodeCounts) {
            initValues(maxNodeCounts.length);
            this.maxNodeCounts = maxNodeCounts.clone();
            return this;
        }

        public Builder setPoolType(PoolType poolType) {
            this.poolType = poolType;
            return this;
        }

        public Builder setPoolId(String poolId) {
            this.poolId = poolId;
            this.poolType = PoolType.OTHERS;
            return this;
        }

        public Builder setAmiType(String amiType) {
            this.amiType = amiType;
            return this;
        }


        public Builder setAmiTypes(String[] amiTypes) {
            initValues(amiTypes.length);
            this.amiTypes = amiTypes;
            return this;
        }

        public Builder setMachineType(String machineType) {
            this.machineType = machineType;
            return this;
        }

        public Builder setMachineType(int index, String machineType) {
            machineTypes[index] = machineType;
            return this;
        }

        public Builder setVpcType(VpcType vpcType) {
            this.vpcType = vpcType;
            return this;
        }

        public EksConfig build(){
            return new EksConfig(this);
        }
    }

    final private static Collection<Parameter> nodePoolParameters  = initNodePoolParameters();
    //Storing Static Map of Parameters
    private static Collection<Parameter> initNodePoolParameters(){
        Collection<Parameter> nodePoolParameters = new ArrayList<Parameter>();
        String key1 = "ClusterName";
        String value1 = "";
        String key2 = "ClusterControlPlaneSecurityGroup";
        //Choose one among The security group of the cluster control plane used while creating cluster
        String value2 = "";
        String key3 = "NodeGroupName";
        String value3 = "";
        String key4 = "NodeAutoScalingGroupMinSize";
        String value4 = "";
        String key5 = "NodeAutoScalingGroupMaxSize";
        String value5 = "";
        String key6 = "NodeInstanceType";
        String value6 = "";
        String key7 = "NodeImageId";
        String value7 = "";
        String key8 = "KeyName";
        String value8 = ""; //The EC2 Key Pair to allow SSH access to the instances
        String key9 = "VpcId";
        String value9 = "";
        String key10 = "Subnets";
        String value10 = "";
        Parameter param1 = new Parameter().withParameterKey(key1).withParameterValue(value1);
        Parameter param2 = new Parameter().withParameterKey(key2).withParameterValue(value2);
        Parameter param3 = new Parameter().withParameterKey(key3).withParameterValue(value3);
        Parameter param4 = new Parameter().withParameterKey(key4).withParameterValue(value4);
        Parameter param5 = new Parameter().withParameterKey(key5).withParameterValue(value5);
        Parameter param6 = new Parameter().withParameterKey(key6).withParameterValue(value6);
        Parameter param7 = new Parameter().withParameterKey(key7).withParameterValue(value7);
        Parameter param8 = new Parameter().withParameterKey(key8).withParameterValue(value8);
        Parameter param9 = new Parameter().withParameterKey(key9).withParameterValue(value9);
        Parameter param10 = new Parameter().withParameterKey(key10).withParameterValue(value10);
        nodePoolParameters.add(param1);
        nodePoolParameters.add(param2);
        nodePoolParameters.add(param3);
        nodePoolParameters.add(param4);
        nodePoolParameters.add(param5);
        nodePoolParameters.add(param6);
        nodePoolParameters.add(param7);
        nodePoolParameters.add(param8);
        nodePoolParameters.add(param9);
        nodePoolParameters.add(param10);
        return nodePoolParameters;
    }

    public  List<Parameter> getNodePoolParameters() {
        // Two level shallow Cloning => Deep Cloning
        return nodePoolParameters.stream().map(parameter ->  new Parameter().withParameterKey(parameter.getParameterKey())
        .withParameterValue(parameter.getParameterValue())).collect(Collectors.toList());
    }
}
