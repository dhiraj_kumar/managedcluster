package com.nirmata.provider.managedcluster.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;


public class AzureUsageDeserializer  extends StdDeserializer<AzureUsage> {

    private static final Logger logger = LoggerFactory.getLogger(AzureUsageDeserializer.class);
    private static final long serialVersionUID = -960355912484502532L;

    public AzureUsageDeserializer() {
        super(AzureUsage.class);
    }

    @Override
    public AzureUsage deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        int currentValue = Integer.valueOf (node.get("currentValue").asText());
        int limit = Integer.valueOf(node.get("limit").asText());
        JsonNode nameNode = node.get("name");
        String  value = nameNode.get("value").asText();
        String localizedValue = nameNode.get("localizedValue").asText();
        String unit = node.get("unit").asText();
        return new AzureUsage(currentValue, limit, value,localizedValue,unit);
    }
}

