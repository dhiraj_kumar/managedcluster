package com.nirmata.provider.managedcluster.util;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class VirtualMachineSizeDeserializer  extends StdDeserializer<VirtualMachineSize> {

    private static final Logger logger = LoggerFactory.getLogger(VirtualMachineSizeDeserializer.class);
    private static final long serialVersionUID = -960355912464502532L;

    public VirtualMachineSizeDeserializer() {
        super(VirtualMachineSize.class);
    }

    @Override
    public VirtualMachineSize deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        int maxDataDiskCount = Integer.valueOf (node.get("maxDataDiskCount").asText());
        int memoryInMB = Integer.valueOf(node.get("memoryInMB").asText());
        int numberOfCores = Integer.valueOf(node.get("numberOfCores").asText());
        int osDiskSizeInMB = Integer.valueOf(node.get("osDiskSizeInMB").asText());
        int resourceDiskSizeInMB = Integer.valueOf(node.get("resourceDiskSizeInMB").asText());
        String name = node.get("name").asText();
        return new VirtualMachineSize( name, numberOfCores,  osDiskSizeInMB, resourceDiskSizeInMB, memoryInMB, maxDataDiskCount);
    }
}
