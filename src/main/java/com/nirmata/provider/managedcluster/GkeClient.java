package com.nirmata.provider.managedcluster;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.container.Container;
import com.nirmata.provider.managedcluster.util.GkeConfig;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Proxy;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GkeClient extends BaseClient {
    private static final Logger logger = LoggerFactory.getLogger(GkeClient.class);

    private static final HttpTransport httpTransport = initHttpTransport();
    private static HttpTransport initHttpTransport(){
        String host = System.getProperty("http.proxyHost");
        HttpTransport httpTransport = null;
        if(host!= null) {
            int port = Integer.valueOf(System.getProperty("http.proxyPort"));
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
            httpTransport = new NetHttpTransport.Builder().setProxy(proxy).build();
        }
        else{
            try {
                httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            }
            catch (GeneralSecurityException | IOException e) {
                logger.error(e.getMessage());
            }
        }
        return httpTransport;
    }

    private static  final JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
    private final GkeConfig config;
    private final Container container;

    public GkeConfig getConfig() {
        return config;
    }

    public Container getContainer() {
        return container;
    }

    public GkeClient(GkeConfig config) throws GeneralSecurityException, IOException {
        this.config = config;
        GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream(config.getCredentialFile()), httpTransport, jsonFactory)
                .createScoped(Arrays.asList("https://www.googleapis.com/auth/cloud-platform"));
        container  = new Container.Builder(httpTransport, jsonFactory, credential).setApplicationName(config.getApplicationName()).build();

    }

}

