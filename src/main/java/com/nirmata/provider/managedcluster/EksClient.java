package com.nirmata.provider.managedcluster;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.PredefinedClientConfigurations;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudformation.AmazonCloudFormation;
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.eks.AmazonEKS;
import com.amazonaws.services.eks.AmazonEKSClientBuilder;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.nirmata.provider.managedcluster.util.EksConfig;
import javafx.util.Pair;


public final class EksClient extends BaseClient {
    private final EksConfig eksConfig;
    private final AmazonEC2 ec2;
    private final AmazonEKS eks;
    private final AmazonCloudFormation amazonCloudFormation;
    private final AmazonIdentityManagement amazonIdentityManagement;

    private static final Pair<String,Integer> proxy=  initProxy();

    private static Pair<String,Integer> initProxy(){
        String host = System.getProperty("http.proxyHost");
        Pair proxy = null;
        if(host!= null) {
            int port = Integer.valueOf(System.getProperty("http.proxyPort"));
            proxy = new Pair<>(host,port);
        }
        return proxy;
    }

    public EksConfig getConfig() {
        return eksConfig;
    }


    public EksClient(EksConfig eksConfig) throws CloneNotSupportedException {
        this.eksConfig = eksConfig;
        AWSCredentials awsCredentials = new BasicAWSCredentials(eksConfig.getCredential().getAccessKey(), eksConfig.getCredential().getSecretKey());
        AWSCredentialsProvider awsCredentialsProvider = new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return awsCredentials;
            }
            @Override
            public void refresh() {
            }
        };
        ClientConfiguration clientConfiguration = null;
        if(proxy!=null){
            clientConfiguration = new ClientConfiguration().withProxyHost(proxy.getKey()).withProxyPort(proxy.getValue());
        }
        else {
            clientConfiguration = PredefinedClientConfigurations.defaultConfig();
        }
        ec2 =  AmazonEC2ClientBuilder.standard()
                .withCredentials(awsCredentialsProvider).withRegion(eksConfig.getRegion()).withClientConfiguration(clientConfiguration).build();
        eks = AmazonEKSClientBuilder.standard()
                .withCredentials(awsCredentialsProvider).withRegion(eksConfig.getRegion()).withClientConfiguration(clientConfiguration).build();
        amazonCloudFormation  = AmazonCloudFormationClient.builder()
                .withCredentials(awsCredentialsProvider)
                .withRegion(eksConfig.getRegion()).withClientConfiguration(clientConfiguration).build();
        amazonIdentityManagement = AmazonIdentityManagementClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(eksConfig.getRegion()).withClientConfiguration(clientConfiguration).build();
    }
    
    public AmazonEC2 getEc2() {
        return ec2;
    }

    public AmazonEKS getEks() {
        return eks;
    }

    public AmazonCloudFormation getAmazonCloudFormation() {
        return amazonCloudFormation;
    }

    public AmazonIdentityManagement getAmazonIdentityManagement() {
        return amazonIdentityManagement;
    }
}

